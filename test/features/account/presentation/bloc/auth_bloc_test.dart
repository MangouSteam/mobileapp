import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:ltqd/core/failure.dart';
import 'package:ltqd/features/account/domain/entities/user.dart';
import 'package:ltqd/features/account/domain/repositories/auth_user_repository.dart';
import 'package:ltqd/features/account/domain/usecases/get_auth_stream.dart';
import 'package:ltqd/features/account/domain/usecases/register_user.dart';
import 'package:ltqd/features/account/domain/usecases/signin_user.dart';
import 'package:ltqd/features/account/presentation/bloc/bloc/auth_bloc.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:mocktail/mocktail.dart';

class MockSignInUser extends Mock implements SignInUser {
  final AuthUserRepository repository;

  MockSignInUser(this.repository);
}

class MockRegisterUser extends Mock implements RegisterUser {
  final AuthUserRepository repository;

  MockRegisterUser(this.repository);
}

class MockGetAuthUserStream extends Mock implements GetAuthUserStream {
  final AuthUserRepository repository;

  MockGetAuthUserStream(this.repository);
}

class MockAuthUserRepository extends Mock implements AuthUserRepository {}

void main() {
  final userRegistered = LTQDUser(
    displayName: 'TEST',
    email: 'registered@test.com',
  );
  final userNotRegistered = LTQDUser(
    displayName: 'TEST2',
    email: 'notregistered@test.com',
  );
  final badCandidateRegistration = LTQDUser(
    displayName: 'TEST3',
    email: 'a',
  );

  MockAuthUserRepository mockAuthUserRepository = MockAuthUserRepository();
  MockSignInUser mockSignInUser = MockSignInUser(mockAuthUserRepository);
  MockRegisterUser mockRegisterUser = MockRegisterUser(mockAuthUserRepository);
  MockGetAuthUserStream mockGetAuthUserStream =
      MockGetAuthUserStream(mockAuthUserRepository);

  group('Unauthenticate', () {
    blocTest<AuthBloc, AuthState>(
      'emits [] at first.',
      build: () {
        AuthBloc authBloc = AuthBloc(
            signInUser: mockSignInUser,
            registerUser: mockRegisterUser,
            getAuthUserStream: mockGetAuthUserStream);
        return authBloc;
      },
      act: (bloc) => {bloc},
      expect: () => <AuthState>[],
    );

    blocTest<AuthBloc, AuthState>(
      'emits [Unauthenticated] when Unauthenticate is emitted',
      build: () {
        AuthBloc authBloc = AuthBloc(
            signInUser: mockSignInUser,
            registerUser: mockRegisterUser,
            getAuthUserStream: mockGetAuthUserStream);
        return authBloc;
      },
      act: (bloc) {
        bloc.add(Unauthenticate());
        bloc.close();
      },
      expect: () => <AuthState>[Unauthenticated()],
    );
  });

  group('Authenticating', () {
    blocTest<AuthBloc, AuthState>(
      '''emits [Authenticated] when Authenticating is emitted with a registered
       user and provider is email password''',
      build: () {
        AuthBloc authBloc = AuthBloc(
            signInUser: mockSignInUser,
            registerUser: mockRegisterUser,
            getAuthUserStream: mockGetAuthUserStream);
        return authBloc;
      },
      act: (bloc) {
        when(() => mockSignInUser.call(SignInUserParams(
                provider: AuthProvider.emailPassword,
                data:
                    SignInData(email: userRegistered.email, password: '123'))))
            .thenAnswer((invocation) async {
          return await Future.value(Right(userRegistered));
        });
        bloc.add(Authenticating(
            provider: AuthProvider.emailPassword,
            email: userRegistered.email,
            password: '123'));
      },
      expect: () => <AuthState>[Authenticated(userRegistered)],
    );

    blocTest<AuthBloc, AuthState>(
      '''emits [Authenticated] when Authenticating is emitted with a non registered
       user and provider is email password''',
      build: () {
        AuthBloc authBloc = AuthBloc(
            signInUser: mockSignInUser,
            registerUser: mockRegisterUser,
            getAuthUserStream: mockGetAuthUserStream);
        return authBloc;
      },
      act: (bloc) {
        when(() => mockSignInUser.call(SignInUserParams(
            provider: AuthProvider.emailPassword,
            data: SignInData(
                email: userNotRegistered.email,
                password: '123')))).thenAnswer((invocation) async {
          return await Future.value(Left(AuthFailure()));
        });
        bloc.add(Authenticating(
            provider: AuthProvider.emailPassword,
            email: userNotRegistered.email,
            password: '123'));
      },
      tearDown: () {
        reset(mockSignInUser);
      },
      expect: () => <AuthState>[Unauthenticated()],
    );

    blocTest<AuthBloc, AuthState>(
      '''emits [Authenticated] when Authenticating is emitted with registered user 
      and Authprovider is facebook''',
      build: () {
        AuthBloc authBloc = AuthBloc(
            signInUser: mockSignInUser,
            registerUser: mockRegisterUser,
            getAuthUserStream: mockGetAuthUserStream);
        return authBloc;
      },
      act: (bloc) {
        when(() => mockSignInUser.call(SignInUserParams(
              provider: AuthProvider.facebook,
              data: SignInData(email: null, password: null),
            ))).thenAnswer((invocation) async {
          return await Future.value(Right(userRegistered));
        });

        bloc.add(Authenticating(provider: AuthProvider.facebook));
      },
      tearDown: () {
        reset(mockSignInUser);
      },
      expect: () => <AuthState>[Authenticated(userRegistered)],
    );

    blocTest<AuthBloc, AuthState>(
      '''emits [Authenticated] when Authenticating is emitted with notregistered user 
      and Authprovider is facebook''',
      build: () {
        AuthBloc authBloc = AuthBloc(
            signInUser: mockSignInUser,
            registerUser: mockRegisterUser,
            getAuthUserStream: mockGetAuthUserStream);
        return authBloc;
      },
      act: (bloc) {
        when(() => mockSignInUser.call(SignInUserParams(
              provider: AuthProvider.facebook,
              data: SignInData(email: null, password: null),
            ))).thenAnswer((invocation) async {
          return await Future.value(Left(AuthFailure()));
        });

        bloc.add(Authenticating(provider: AuthProvider.facebook));
      },
      tearDown: () {
        reset(mockSignInUser);
      },
      expect: () => <AuthState>[Unauthenticated()],
    );

    blocTest<AuthBloc, AuthState>(
      '''emits [Authenticated] when Authenticating is emitted with registered user 
      and Authprovider is apple''',
      build: () {
        AuthBloc authBloc = AuthBloc(
            signInUser: mockSignInUser,
            registerUser: mockRegisterUser,
            getAuthUserStream: mockGetAuthUserStream);
        return authBloc;
      },
      act: (bloc) {
        when(() => mockSignInUser.call(SignInUserParams(
              provider: AuthProvider.apple,
              data: SignInData(email: null, password: null),
            ))).thenAnswer((invocation) async {
          return await Future.value(Right(userRegistered));
        });

        bloc.add(Authenticating(provider: AuthProvider.apple));
      },
      tearDown: () {
        reset(mockSignInUser);
      },
      expect: () => <AuthState>[Authenticated(userRegistered)],
    );

    blocTest<AuthBloc, AuthState>(
      '''emits [Authenticated] when Authenticating is emitted with notregistered user 
      and Authprovider is apple''',
      build: () {
        AuthBloc authBloc = AuthBloc(
            signInUser: mockSignInUser,
            registerUser: mockRegisterUser,
            getAuthUserStream: mockGetAuthUserStream);
        return authBloc;
      },
      act: (bloc) {
        when(() => mockSignInUser.call(SignInUserParams(
              provider: AuthProvider.apple,
              data: SignInData(email: null, password: null),
            ))).thenAnswer((invocation) async {
          return await Future.value(Left(AuthFailure()));
        });

        bloc.add(Authenticating(provider: AuthProvider.apple));
      },
      tearDown: () {
        reset(mockSignInUser);
      },
      expect: () => <AuthState>[Unauthenticated()],
    );
  });

  group('AuthenticateFailed', () {
    blocTest<AuthBloc, AuthState>(
      'emits [Unauthenticated] when AuthenticateFailed is emitted',
      build: () {
        AuthBloc authBloc = AuthBloc(
            signInUser: mockSignInUser,
            registerUser: mockRegisterUser,
            getAuthUserStream: mockGetAuthUserStream);

        return authBloc;
      },
      act: (bloc) {
        bloc.add(AuthenticateFailed());
        bloc.close();
      },
      expect: () => <AuthState>[Unauthenticated()],
    );
  });

  group('AuthenticateSuccess', () {
    blocTest<AuthBloc, AuthState>(
      'emits [Authenticated] when AuthenticateSuccess is emitted',
      build: () {
        AuthBloc authBloc = AuthBloc(
            signInUser: mockSignInUser,
            registerUser: mockRegisterUser,
            getAuthUserStream: mockGetAuthUserStream);
        return authBloc;
      },
      act: (bloc) {
        bloc.add(AuthenticateSuccess(userRegistered));
        bloc.close();
      },
      expect: () => <AuthState>[Authenticated(userRegistered)],
    );
  });

  group('Registering', () {
    blocTest<AuthBloc, AuthState>(
      'emits [Unauthenticated] when Registering with bad input is emitted',
      build: () {
        AuthBloc authBloc = AuthBloc(
            signInUser: mockSignInUser,
            registerUser: mockRegisterUser,
            getAuthUserStream: mockGetAuthUserStream);

        return authBloc;
      },
      act: (bloc) {
        when(() => mockRegisterUser.call(RegisterUserParams(
              data: RegisterData(
                  email: badCandidateRegistration.email, password: ''),
            ))).thenAnswer((invocation) async {
          return await Future.value(Left(AuthFailure()));
        });
        bloc.add(Registering(
            provider: AuthProvider.emailPassword,
            email: badCandidateRegistration.email,
            password: ''));
      },
      expect: () => <AuthState>[Unauthenticated()],
    );

    blocTest<AuthBloc, AuthState>(
      'emits [Authenticated] when Registering with good input',
      build: () {
        AuthBloc authBloc = AuthBloc(
            signInUser: mockSignInUser,
            registerUser: mockRegisterUser,
            getAuthUserStream: mockGetAuthUserStream);

        return authBloc;
      },
      act: (bloc) {
        when(() => mockRegisterUser.call(RegisterUserParams(
              data: RegisterData(email: userNotRegistered.email, password: ''),
            ))).thenAnswer((invocation) async {
          return await Future.value(Right(userNotRegistered));
        });
        bloc.add(Registering(
            provider: AuthProvider.emailPassword,
            email: userNotRegistered.email,
            password: ''));
      },
      expect: () => <AuthState>[Authenticated(userNotRegistered)],
    );
  });
}
