import 'package:flutter_test/flutter_test.dart';
import 'package:ltqd/features/account/presentation/bloc/child_form/child_form_bloc.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:ltqd/features/account/presentation/widgets/forms/form_models.dart/child_form_model.dart';
import 'package:ltqd/features/tracking/domain/object_values/gender.dart';
import 'package:ltqd/features/tracking/domain/usecases/add_new_child.dart';
import 'package:mocktail/mocktail.dart';

class MockAddNewChild extends Mock implements AddNewChild {}

void main() {
  MockAddNewChild mockAddNewChild;
  ChildFormModel childFormModelTestSuccess =
      ChildFormModel(firstName: 'Philippe', gender: Gender.boy);

  mockAddNewChild = MockAddNewChild();

  group('Initial', () {
    blocTest<ChildFormBloc, ChildFormState>(
      'emits [] at first.',
      build: () => ChildFormBloc(addNewChild: mockAddNewChild),
      act: (bloc) => {bloc},
      expect: () => <ChildFormState>[],
    );
  });

  group('ChildFormSubmitted', () {
    blocTest<ChildFormBloc, ChildFormState>(
      'Event ChildFormSubmit should yield FormSubmitInProgress and ',
      build: () => ChildFormBloc(addNewChild: mockAddNewChild),
      act: (bloc) => {
        bloc.add(ChildFormSubmitted(childFormModel: childFormModelTestSuccess))
      },
      expect: () => <ChildFormState>[ChildFormSubmitInProgress()],
    );
  });

  group('ChildFormReset', () {
    blocTest<ChildFormBloc, ChildFormState>(
      'Event ChildFormReset should yield ChildFormInitial',
      build: () => ChildFormBloc(addNewChild: mockAddNewChild),
      act: (bloc) => {bloc.add(ChildFormReset())},
      expect: () => <ChildFormState>[ChildFormInitial()],
    );
  });
}
