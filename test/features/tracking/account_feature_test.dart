// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_import, directives_ordering

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import './step/the_user_is_loaded.dart';
import './step/the_app_is_running.dart';
import './step/i_see_text.dart';
import './step/i_click_button.dart';

void main() {
  group('Account', () {
    testWidgets('Page is loaded and it should have a user loaded', (tester) async {
      await theUserIsLoaded(tester);
      await theAppIsRunning(tester);
      await iSeeText(tester, 'Changer le mot de passe');
      await iSeeText(tester, 'Ajouter une enfant');
      await iSeeText(tester, 'Contacter le support');
      await iSeeText(tester, 'Se déconnecter');
    });
    testWidgets('When I click Se déconnecter I should be redirected to Signin', (tester) async {
      await theUserIsLoaded(tester);
      await theAppIsRunning(tester);
      await iSeeText(tester, 'changer le mot de passe');
      await iClickButton(tester, 'Se déconnecter');
      await iSeeText(tester, 'Se connecter');
    });
    testWidgets('When I click Changer le mot de passe I should be redirected to changer le mot de passe', (tester) async {
      await theUserIsLoaded(tester);
      await theAppIsRunning(tester);
      await iSeeText(tester, 'changer le mot de passe');
      await iClickButton(tester, 'changer le mot de passe');
      await iSeeText(tester, 'Changer le mot de passe');
    });
    testWidgets('When I click Ajouter un enfant I should be redirected to changer le mot de passe', (tester) async {
      await theUserIsLoaded(tester);
      await theAppIsRunning(tester);
      await iSeeText(tester, 'Ajouter un enfant');
      await iClickButton(tester, 'Ajouter un enfant');
      await iSeeText(tester, 'Ajouter un enfant');
    });
  });
}
