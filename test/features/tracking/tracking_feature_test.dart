// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_import, directives_ordering

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import './step/there_are_no_children_for_the_given_user.dart';
import './step/the_app_is_running.dart';
import './step/i_see_text.dart';

void main() {
  group('Tracking', () {
    testWidgets('Page is loaded and there are no children', (tester) async {
      await thereAreNoChildrenForTheGivenUser(tester);
      await theAppIsRunning(tester);
      await iSeeText(tester, 'Sommeil');
    });
    testWidgets('Page is loaded and the ActiveChild has no sleepTimer.currentlyTicking', (tester) async {
      await theAppIsRunning(tester);
    });
    testWidgets('Page is loaded and the ActiveChild has a sleepTimer.currentlyTicking', (tester) async {
      await theAppIsRunning(tester);
    });
    testWidgets('Page is loaded and the child just changed', (tester) async {
      await theAppIsRunning(tester);
    });
    testWidgets('Button Start should start the chrono only for the active child', (tester) async {
      await theAppIsRunning(tester);
    });
    testWidgets('Button Stop should stop the chrono only for the active child', (tester) async {
      await theAppIsRunning(tester);
    });
  });
}
