import 'package:flutter_test/flutter_test.dart';
import 'package:ltqd/features/tracking/data/models/children.dart';
import 'package:ltqd/features/tracking/domain/entities/children.dart';
import 'package:ltqd/features/tracking/domain/object_values/gender.dart';

void main() {
  final tChildrenModel = ChildrenModel(
    firstName: 'Martin',
    gender: Gender.boy,
    id: 'a',
    initials: 'MA',
  );
  test('should be a subclass of Children', () async {
    expect(tChildrenModel, isA<Children>());
  });
}
