import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:ltqd/features/tracking/domain/repositories/children_repository.dart';
import 'package:ltqd/features/tracking/domain/usecases/get_children_stream.dart';
import 'package:ltqd/features/tracking/presentation/bloc/children/children_bloc.dart';
import 'package:mocktail/mocktail.dart';

class MockGetChildrenStream extends Mock implements GetChildrenStream {
  final ChildrenRepository repository;

  MockGetChildrenStream(this.repository);
}

class MockChildrenRepository extends Mock implements ChildrenRepository {}

void main() {
  MockChildrenRepository mockChildrenRepository = MockChildrenRepository();
  MockGetChildrenStream mockGetChildrenStream =
      MockGetChildrenStream(mockChildrenRepository);

  group('initial', () {
    blocTest<ChildrenBloc, ChildrenState>(
      'emits [] at first.',
      build: () {
        return ChildrenBloc(getChildrenStream: mockGetChildrenStream);
      },
      act: (bloc) => {bloc},
      expect: () => <ChildrenState>[],
    );
  });

  group('ChildrenDataLoading', () {
    blocTest<ChildrenBloc, ChildrenState>(
      'emits [ChildrenLoadInProgress] when ChildrenDataLoading is received.',
      build: () {
        return ChildrenBloc(getChildrenStream: mockGetChildrenStream);
      },
      act: (bloc) => {bloc.add(ChildrenDataLoading())},
      expect: () => <ChildrenState>[
        ChildrenLoadInProgress(),
      ],
    );
  });

  group('ChildrenDataLoaded', () {
    blocTest<ChildrenBloc, ChildrenState>(
      'emits [ChildrenLoadSuccess] when a ChildrenDataLoaded(children: [])',
      build: () {
        return ChildrenBloc(getChildrenStream: mockGetChildrenStream);
      },
      act: (bloc) => {bloc.add(ChildrenDataLoaded(children: []))},
      wait: const Duration(milliseconds: 1000),
      expect: () => <ChildrenState>[
        ChildrenLoadSuccess(children: []),
      ],
    );
  });
}
