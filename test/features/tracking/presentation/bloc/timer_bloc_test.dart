import 'package:bloc_test/bloc_test.dart';
import 'package:ltqd/features/tracking/data/datasources/ticker.dart';
import 'package:ltqd/features/tracking/presentation/bloc/timer/timer_bloc.dart';
import 'package:mocktail/mocktail.dart';
import 'package:test/test.dart';

class MockTicker extends Mock implements Ticker {
  @override
  Stream<int> tick({required int ticks}) {
    return Stream.value(1);
  }

  cancel() {
    return;
  }
}

void main() {
  MockTicker mockTicker;
  TimerBloc timerBloc;
  mockTicker = MockTicker();
  timerBloc = TimerBloc(ticker: mockTicker);

  setUp(() {
    timerBloc = TimerBloc(ticker: mockTicker);
  });

  tearDown(() {
    timerBloc.close();
  });

  group('Initial', () {
    blocTest<TimerBloc, TimerState>(
      'emits [] at first.',
      build: () => timerBloc,
      expect: () => <TimerState>[],
    );
  });

  group('TimerStarted', () {
    blocTest<TimerBloc, TimerState>(
      'emits (TimerRunInProgress(0), TimerRunInProgress(1)) when TimerStarted.',
      build: () => timerBloc,
      act: (bloc) => {bloc.add(TimerStarted(duration: 0))},
      wait: const Duration(milliseconds: 100),
      expect: () => <TimerState>[
        TimerRunInProgress(0),
        TimerRunInProgress(1),
      ],
    );
  });

  group('TimerStopped', () {
    blocTest<TimerBloc, TimerState>(
      'emits no state when TimerRunStop is triggered without a TimerRunInProgress',
      build: () => timerBloc,
      act: (bloc) {
        bloc.add(TimerStopped(duration: 1));
      },
      wait: const Duration(milliseconds: 300),
      expect: () => <TimerState>[],
    );

    blocTest<TimerBloc, TimerState>(
      'emits TimerRunStop when TimerRunStop is triggered with a TimerRunInProgress',
      build: () => timerBloc,
      act: (bloc) {
        bloc.add(TimerStarted(duration: 0));
        bloc.add(TimerStopped(duration: 0));
      },
      wait: const Duration(milliseconds: 1000),
      expect: () => <TimerState>[
        TimerRunInProgress(0),
        TimerRunStop(0),
        TimerRunInProgress(1)
      ],
    );
  });

  group('TimerReset', () {
    blocTest<TimerBloc, TimerState>(
      """emits TimerInitial state when TimerReset is triggered without a
      TimerRunInProgress""",
      build: () => timerBloc,
      act: (bloc) {
        bloc.add(TimerReset());
      },
      wait: const Duration(milliseconds: 300),
      expect: () => <TimerState>[TimerInitial()],
    );

    blocTest<TimerBloc, TimerState>(
      'emits TimerInitial when reset',
      build: () => timerBloc,
      act: (bloc) {
        bloc.add(TimerStarted(duration: 0));
        bloc.add(TimerReset());
      },
      wait: const Duration(milliseconds: 1000),
      expect: () => <TimerState>[
        TimerRunInProgress(0),
        TimerInitial(),
        TimerRunInProgress(1)
      ],
    );
  });

  group('TimerPaused', () {
    blocTest<TimerBloc, TimerState>(
      'no emits if no currentTimer',
      build: () => timerBloc,
      act: (bloc) {
        bloc.add(TimerPaused(duration: 1));
      },
      expect: () => <TimerState>[],
    );

    blocTest<TimerBloc, TimerState>(
      'should emit TimerRunPause previous state if currentTimer',
      build: () => timerBloc,
      act: (bloc) {
        bloc.add(TimerStarted(duration: 0));
        bloc.add(TimerPaused(duration: 0));
      },
      expect: () => <TimerState>[
        TimerRunInProgress(0),
        TimerRunPause(0),
        TimerRunInProgress(1),
      ],
    );
  });

  group('TimerResumed', () {
    blocTest<TimerBloc, TimerState>(
      'no emits if no currentTime paused',
      build: () => timerBloc,
      act: (bloc) {
        bloc.add(TimerPaused(duration: 1));
      },
      expect: () => <TimerState>[],
    );

    blocTest<TimerBloc, TimerState>(
      'should emit TimerRunInProgress if previous state is TimerRunPause',
      build: () => timerBloc,
      act: (bloc) {
        bloc.add(TimerStarted(duration: 0));
        bloc.add(TimerPaused(duration: 1));
        bloc.add(TimerResumed(duration: 1));
      },
      expect: () => <TimerState>[
        TimerRunInProgress(0),
        TimerRunPause(0),
        TimerRunInProgress(0),
        TimerRunInProgress(1)
      ],
    );
  });

  group('TimerTicked', () {
    blocTest<TimerBloc, TimerState>(
      'should emit TimerRunInProgress when ticked',
      build: () => timerBloc,
      act: (bloc) {
        bloc.add(TimerTicked(duration: 0));
      },
      expect: () => <TimerState>[
        TimerRunInProgress(0),
      ],
    );
  });
}
