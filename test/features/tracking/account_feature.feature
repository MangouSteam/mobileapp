Feature: Account

    Scenario: Page is loaded and it should have a user loaded
        Given the user is loaded
        Given the app is running
        Then I see {'Changer le mot de passe'} text
        And  I see {'Ajouter une enfant'} text
        And I see {'Contacter le support'} text
        And I see {'Se déconnecter'} text

    Scenario: When I click Se déconnecter I should be redirected to Signin
        Given The user is loaded
        Given the app is running
        Then I see {'changer le mot de passe'} text
        When I click {'Se déconnecter'} button
        Then I see {'Se connecter'} text


    Scenario: When I click Changer le mot de passe I should be redirected to changer le mot de passe
        Given The user is loaded
        Given the app is running
        Then I see {'changer le mot de passe'} text
        When I click {'changer le mot de passe'} button
        Then I see {'Changer le mot de passe'} text

    Scenario: When I click Ajouter un enfant I should be redirected to changer le mot de passe
        Given The user is loaded
        Given the app is running
        Then I see {'Ajouter un enfant'} text
        When I click {'Ajouter un enfant'} button
        Then I see {'Ajouter un enfant'} text