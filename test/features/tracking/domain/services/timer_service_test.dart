import 'package:flutter_test/flutter_test.dart';
import 'package:ltqd/features/tracking/domain/services/timer_service.dart';

void main() {
  test('it should return 0 when same dates', () {
    var actual = TimerService.getDurationInSeconds(DateTime.now());
    expect(actual, 0);
  });

  test('it should return 100 when date is 100 seconds after DateTime.now', () {
    var actual = TimerService.getDurationInSeconds(
        DateTime.now().subtract(Duration(seconds: 100)));
    expect(actual, 100);
  });

  test('it should return 0 if date after DateTime.now()', () {
    var actual = TimerService.getDurationInSeconds(
        DateTime.now().add(Duration(seconds: 100)));
    expect(actual, 0);
  });

  test('it should return int', () {
    var actual = TimerService.getDurationInSeconds(DateTime.now());
    expect(actual is int, true);
  });
}
