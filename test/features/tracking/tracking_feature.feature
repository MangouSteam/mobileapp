Feature: Tracking

    Scenario: Page is loaded and there are no children
        Given there are no children for the given user
        Given the app is running
        Then I see {'Sommeil'} text

    Scenario: Page is loaded and the ActiveChild has no sleepTimer.currentlyTicking
        Given the app is running

    Scenario: Page is loaded and the ActiveChild has a sleepTimer.currentlyTicking
        Given the app is running


    Scenario: Page is loaded and the child just changed
        Given the app is running

    Scenario: Button Start should start the chrono only for the active child
        Given the app is running


    Scenario: Button Stop should stop the chrono only for the active child
        Given The app is running

