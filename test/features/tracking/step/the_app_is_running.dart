import 'package:flutter_test/flutter_test.dart';
import 'package:ltqd/core/environment.dart';
import 'package:ltqd/main.dart';
import 'package:ltqd/core/injection_container.dart' as di;

Future<void> theAppIsRunning(WidgetTester tester) async {
  di.init(Environment.test);
  await tester.pumpWidget(MyApp());
  await tester.pumpAndSettle();
}
