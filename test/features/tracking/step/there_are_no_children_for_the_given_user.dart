import 'package:flutter_test/flutter_test.dart';
import 'package:ltqd/core/usecases/usecase.dart';
import 'package:ltqd/features/tracking/domain/usecases/get_children_stream.dart';
import 'package:mocktail/mocktail.dart';

class MockGetChildrenStream extends Mock implements GetChildrenStream {}

Future<void> thereAreNoChildrenForTheGivenUser(WidgetTester tester) async {
  final mockGetChildrenStream = MockGetChildrenStream();
  when(() => mockGetChildrenStream.call(NoParams())).thenAnswer((_) {
    return Stream.value([]);
  });

  final a = mockGetChildrenStream.call(NoParams());
  final b = 1;
}
