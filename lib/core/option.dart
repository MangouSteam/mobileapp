class Option {
  final String label;
  final String asset;

  Option({required this.label, required this.asset});
}
