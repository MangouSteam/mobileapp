import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  Failure([List properties = const <dynamic>[]]) : super();
}

class ServerFailure extends Failure {
  @override
  List<Object?> get props => throw UnimplementedError();
}

class CacheFailure extends Failure {
  @override
  List<Object?> get props => throw UnimplementedError();
}

class AuthProviderNotImplementedFailure extends Failure {
  @override
  List<Object?> get props => throw UnimplementedError();
}

class AuthFailure extends Failure {
  @override
  List<Object?> get props => throw UnimplementedError();
}

class WeakPasswordAuthFailure extends Failure {
  final String message = 'The password provided is too week';

  @override
  List<Object?> get props => [message];
}

class EmailAlreadyInUseAuthFailure extends Failure {
  final String message = 'The account already exists for that email.';

  @override
  List<Object?> get props => [message];
}

class AccountExistsWithDifferentCredentialAuthFailure extends Failure {
  final String message = 'Account exists with different credential';

  @override
  List<Object?> get props => [message];
}

class UserNotFoundAuthFailure extends Failure {
  final String message = 'User not found';

  @override
  List<Object?> get props => [message];
}

class WrongPasswordAuthFailure extends Failure {
  final String message = 'Wrong password provided for that user';

  @override
  List<Object?> get props => [message];
}
