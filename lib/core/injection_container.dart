import 'package:firebase_auth/firebase_auth.dart';
import 'package:get_it/get_it.dart';
import 'package:ltqd/core/network_info.dart';
import 'package:ltqd/features/account/data/datasources/auth_user_remote_data_source.dart';
import 'package:ltqd/features/account/data/repositories/auth_user_repository_impl.dart';
import 'package:ltqd/features/account/domain/repositories/auth_user_repository.dart';
import 'package:ltqd/features/account/domain/usecases/get_auth_stream.dart';
import 'package:ltqd/features/account/domain/usecases/register_user.dart';
import 'package:ltqd/features/account/domain/usecases/signin_user.dart';
import 'package:ltqd/features/account/presentation/bloc/bloc/auth_bloc.dart';
import 'package:ltqd/features/account/presentation/bloc/child_form/child_form_bloc.dart';
import 'package:ltqd/features/tracking/data/datasources/children/children_local_data_source.dart';
import 'package:ltqd/features/tracking/data/datasources/children/children_remote_data_source.dart';
import 'package:ltqd/features/tracking/data/datasources/sleep_session/sleep_session_local_data.dart';
import 'package:ltqd/features/tracking/data/datasources/sleep_session/sleep_session_remote_data.dart';
import 'package:ltqd/features/tracking/data/datasources/ticker.dart';
import 'package:ltqd/features/tracking/data/repositories/children_repository_impl.dart';
import 'package:ltqd/features/tracking/data/repositories/sleep_sessions_repository_impl.dart';
import 'package:ltqd/features/tracking/domain/repositories/children_repository.dart';
import 'package:ltqd/features/tracking/domain/repositories/sleep_sessions_repository.dart';
import 'package:ltqd/features/tracking/domain/usecases/add_new_child.dart';
import 'package:ltqd/features/tracking/domain/usecases/end_sleep_timer.dart';
import 'package:ltqd/features/tracking/domain/usecases/get_children_stream.dart';
import 'package:ltqd/features/tracking/domain/usecases/start_sleep_timer.dart';
import 'package:ltqd/features/tracking/presentation/bloc/child_active/child_active_bloc.dart';
import 'package:ltqd/features/tracking/presentation/bloc/children/children_bloc.dart';
import 'package:ltqd/features/tracking/presentation/bloc/period/period_bloc.dart';
import 'package:ltqd/features/tracking/presentation/bloc/timer/timer_bloc.dart';
import 'package:ltqd/features/tracking/presentation/bloc/tracking_mediator/tracking_mediator_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

final sl = GetIt.instance;

Future<void> init(String environment) async {
  //! Features - Tracking
  // Bloc
  sl.registerLazySingleton(() => TimerBloc(ticker: Ticker()));
  sl.registerLazySingleton(() => ChildrenBloc(getChildrenStream: sl()));
  sl.registerLazySingleton(() => ChildActiveBloc());
  sl.registerLazySingleton(() => TrackingMediatorBloc(
        startSleepTimer: sl(),
        endSleepTimer: sl(),
      ));
  sl.registerLazySingleton(() => PeriodBloc());
  sl.registerLazySingleton(() => ChildFormBloc(addNewChild: sl()));
  sl.registerLazySingleton(() =>
      AuthBloc(signInUser: sl(), registerUser: sl(), getAuthUserStream: sl()));

  // UseCases)
  sl.registerLazySingleton(() => GetChildrenStream(sl()));
  sl.registerLazySingleton(() => GetAuthUserStream(sl()));
  sl.registerLazySingleton(() => StartSleepTimer(sl()));
  sl.registerLazySingleton(() => EndSleepTimer(sl()));
  sl.registerLazySingleton(() => AddNewChild(sl()));
  sl.registerLazySingleton(() => SignInUser(sl()));
  sl.registerLazySingleton(() => RegisterUser(sl()));

  // Repository
  sl.registerLazySingleton<ChildrenRepository>(() => ChildrenRepositoryImpl(
        remoteDataSource: sl(),
        localDataSource: sl(),
        networkInfo: sl(),
      ));
  sl.registerLazySingleton<SleepSessionsRepository>(
      () => SleepSessionsRepositoryImpl(
            remoteDataSource: sl(),
            localDataSource: sl(),
            networkInfo: sl(),
          ));
  sl.registerLazySingleton<AuthUserRepository>(() => AuthUserRepositoryImpl(
        remoteDataSource: sl(),
        networkInfo: sl(),
      ));

  // DataSource
  sl.registerLazySingleton<ChildrenLocalDataSource>(
      () => ChildrenLocalDataSourceImpl(sharedPreferences: sl()));
  sl.registerLazySingleton<ChildrenRemoteDataSource>(
      () => ChildrenRemoteDataSourceImpl());
  sl.registerLazySingleton<SleepSessionLocalDataSource>(
      () => SleepSessionLocalDataSourceImpl(sharedPreferences: sl()));
  sl.registerLazySingleton<SleepSessionRemoteDataSource>(
      () => SleepSessionRemoteDataSourceImpl());
  sl.registerLazySingleton<AuthUserRemoteDataSource>(
      () => AuthUserRemoteDataSourceImpl());

  //! Core
  sl.registerLazySingleton<FirebaseFirestore>(() => FirebaseFirestore.instance);
  sl.registerLazySingleton<FirebaseAuth>(() => FirebaseAuth.instance);
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

  //! External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => DataConnectionChecker());
}
