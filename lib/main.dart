import 'package:bloc/bloc.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:ltqd/constants/themes.dart';
import 'package:ltqd/core/injection_container.dart' as di;
import 'package:flutter/material.dart';
import 'package:ltqd/features/account/presentation/pages/add_child_page.dart';
import 'package:ltqd/features/shared/presentation/pages/base_page.dart';
import 'package:ltqd/features/shared/presentation/pages/default_page.dart';
import 'package:ltqd/features/tracking/presentation/pages/sleep_tracking_page.dart';
import 'package:ltqd/ltqd_bloc_observer.dart';

import 'core/environment.dart';
import 'features/tracking/presentation/pages/feed_tracking_page.dart';

Future<void> main() async {
  Bloc.observer = LTQDBlocObserver();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await di.init(Environment.prod);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        navigatorKey: navigatorKey,
        title: 'LTQD',
        theme: AppThemes.lightTheme,
        home: BasePage(index: 1),
        routes: <String, WidgetBuilder>{
          '/default_page': (BuildContext context) => DefaultPage(),
          '/sleep_tracking': (BuildContext context) => SleepTrackingPage(),
          '/feed_tracking': (BuildContext context) => FeedTrackingPage(),
          '/analysis': (BuildContext context) => DefaultPage(),
          '/account': (BuildContext context) => DefaultPage(),
          '/add_child_page': (BuildContext context) => AddChildPage(),
        });
  }
}
