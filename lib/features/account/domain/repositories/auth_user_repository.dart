import 'package:dartz/dartz.dart';
import 'package:ltqd/core/failure.dart';
import 'package:ltqd/core/usecases/usecase.dart';
import 'package:ltqd/features/account/domain/entities/user.dart';
import 'package:ltqd/features/account/domain/usecases/register_user.dart';
import 'package:ltqd/features/account/domain/usecases/signin_user.dart';
import 'package:ltqd/features/account/presentation/bloc/bloc/auth_bloc.dart';

abstract class AuthUserRepository {
  Future<Either<Failure, LTQDUser>> signIn(
      {required AuthProvider provider, required SignInData data});

  Future<Either<Failure, LTQDUser>> register({required RegisterData data});

  Stream<LTQDUser?> getAuthUserStream();

  Future<void> signOut(NoParams noParams);
}
