class LTQDUser {
  final String displayName;
  final String email;

  LTQDUser({
    required this.displayName,
    required this.email,
  });
}
