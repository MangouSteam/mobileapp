import 'package:ltqd/core/usecases/usecase.dart';
import 'package:ltqd/features/account/domain/entities/user.dart';
import 'package:ltqd/features/account/domain/repositories/auth_user_repository.dart';

class GetAuthUserStream {
  final AuthUserRepository repository;
  GetAuthUserStream(this.repository);

  Stream<LTQDUser?> call(NoParams params) {
    return repository.getAuthUserStream();
  }
}
