import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:ltqd/core/failure.dart';
import 'package:ltqd/core/usecases/usecase.dart';
import 'package:ltqd/features/account/domain/entities/user.dart';
import 'package:ltqd/features/account/domain/repositories/auth_user_repository.dart';

class RegisterUser extends UseCase<void, RegisterUserParams> {
  final AuthUserRepository repository;
  RegisterUser(this.repository);

  @override
  Future<Either<Failure, LTQDUser>> call(RegisterUserParams params) async {
    return await repository.register(
      data: RegisterData(
        email: params.data.email,
        password: params.data.password,
      ),
    );
  }
}

class RegisterUserParams extends Equatable {
  final RegisterData data;

  RegisterUserParams({required this.data}) : super();

  @override
  List<Object?> get props => [data];
}

class RegisterData extends Equatable {
  final String? email;
  final String? password;

  RegisterData({this.email, this.password});

  @override
  List<Object?> get props => [email, password];
}
