import 'package:ltqd/core/usecases/usecase.dart';
import 'package:ltqd/features/account/domain/repositories/auth_user_repository.dart';

class SignOutUser {
  final AuthUserRepository repository;
  SignOutUser(this.repository);

  Future<void> call(NoParams params) async {
    return await repository.signOut(NoParams());
  }
}
