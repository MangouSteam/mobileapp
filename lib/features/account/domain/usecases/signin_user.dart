import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:ltqd/core/failure.dart';
import 'package:ltqd/core/usecases/usecase.dart';
import 'package:ltqd/features/account/domain/entities/user.dart';
import 'package:ltqd/features/account/domain/repositories/auth_user_repository.dart';
import 'package:ltqd/features/account/presentation/bloc/bloc/auth_bloc.dart';

class SignInUser extends UseCase<void, SignInUserParams> {
  final AuthUserRepository repository;
  SignInUser(this.repository);

  @override
  Future<Either<Failure, LTQDUser>> call(SignInUserParams params) async {
    return await repository.signIn(
      provider: params.provider,
      data: SignInData(
        email: params.data.email,
        password: params.data.password,
      ),
    );
  }
}

class SignInUserParams extends Equatable {
  final AuthProvider provider;
  final SignInData data;

  SignInUserParams({required this.provider, required this.data}) : super();

  @override
  List<Object?> get props => [provider, data];
}

class SignInData extends Equatable {
  final String? email;
  final String? password;

  SignInData({this.email, this.password});

  @override
  List<Object?> get props => [email, password];
}
