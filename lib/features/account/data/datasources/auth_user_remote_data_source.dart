import 'dart:convert';
import 'dart:math';

import 'package:crypto/crypto.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:ltqd/core/failure.dart';
import 'package:ltqd/core/injection_container.dart';
import 'package:ltqd/features/account/domain/usecases/register_user.dart';
import 'package:ltqd/features/account/domain/usecases/signin_user.dart';
import 'package:ltqd/features/account/presentation/bloc/bloc/auth_bloc.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

abstract class AuthUserRemoteDataSource {
  Future<Either<Failure, UserCredential>> signIn(
      {required AuthProvider provider, required SignInData data});

  Future<Either<Failure, UserCredential>> register(
      {required RegisterData data});

  Stream<User?> getAuthUserStream();

  Future<void> signOut();
}

class AuthUserRemoteDataSourceImpl implements AuthUserRemoteDataSource {
  @override
  Future<Either<Failure, UserCredential>> signIn(
      {required AuthProvider provider, required SignInData data}) async {
    switch (provider) {
      case AuthProvider.emailPassword:
        return await _signinWithEmailPassword(
            email: data.email!, password: data.password!);
      case AuthProvider.google:
        return await _signinWithGoogle();
      case AuthProvider.facebook:
        return await _signinWithFacebook();
      case AuthProvider.apple:
        return await _signinWithApple();
      default:
        return Left(AuthFailure());
    }
  }

  @override
  Future<Either<Failure, UserCredential>> register(
      {required RegisterData data}) async {
    return await _registerWithEmailPassword(
        email: data.email!, password: data.password!);
  }

  Future<Either<Failure, UserCredential>> _signinWithApple() async {
    try {
      final rawNonce = generateNonce();
      final nonce = sha256ofString(rawNonce);

      // Request credential for the currently signed in Apple account.
      final appleCredential = await SignInWithApple.getAppleIDCredential(
        scopes: [
          AppleIDAuthorizationScopes.email,
          AppleIDAuthorizationScopes.fullName,
        ],
        nonce: nonce,
      );

      final oauthCredential = OAuthProvider("apple.com").credential(
        idToken: appleCredential.identityToken,
        rawNonce: rawNonce,
      );

      final userCredential =
          await sl<FirebaseAuth>().signInWithCredential(oauthCredential);

      return Right(userCredential);
    } on Exception {
      return Left(AuthFailure());
    }
  }

  Future<Either<Failure, UserCredential>> _signinWithFacebook() async {
    try {
      final LoginResult loginResult = await FacebookAuth.instance.login();

      final OAuthCredential facebookAuthCredential =
          FacebookAuthProvider.credential(loginResult.accessToken!.token);

      final userCredential =
          await sl<FirebaseAuth>().signInWithCredential(facebookAuthCredential);

      return Right(userCredential);
    } on Exception {
      return Left(AuthFailure());
    }
  }

  Future<Either<Failure, UserCredential>> _signinWithGoogle() async {
    try {
      final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

      final GoogleSignInAuthentication googleAuth =
          await googleUser!.authentication;

      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      final userCredential =
          await sl<FirebaseAuth>().signInWithCredential(credential);
      return Right(userCredential);
    } on Exception {
      return Left(AuthFailure());
    }
  }

  Future<Either<Failure, UserCredential>> _signinWithEmailPassword(
      {required String email, required String password}) async {
    try {
      final userCredential = await sl<FirebaseAuth>()
          .signInWithEmailAndPassword(email: email, password: password);
      return Right(userCredential);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        return Left(UserNotFoundAuthFailure());
      } else if (e.code == 'wrong-password') {
        return Left(WrongPasswordAuthFailure());
      }
      return Left(AuthFailure());
    } catch (e) {
      return Left(AuthFailure());
    }
  }

  Future<Either<Failure, UserCredential>> _registerWithEmailPassword(
      {required String email, required String password}) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
      return Right(userCredential);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        return Left(WeakPasswordAuthFailure());
      } else if (e.code == 'email-already-in-use') {
        return Left(EmailAlreadyInUseAuthFailure());
      }
      return Left(AuthFailure());
    } catch (e) {
      return Left(AuthFailure());
    }
  }

  @override
  Stream<User?> getAuthUserStream() {
    return sl<FirebaseAuth>().authStateChanges();
  }

  @override
  Future<void> signOut() {
    return sl<FirebaseAuth>().signOut();
  }
}

String generateNonce([int length = 32]) {
  final charset =
      '0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._';
  final random = Random.secure();
  return List.generate(length, (_) => charset[random.nextInt(charset.length)])
      .join();
}

/// Returns the sha256 hash of [input] in hex notation.
String sha256ofString(String input) {
  final bytes = utf8.encode(input);
  final digest = sha256.convert(bytes);
  return digest.toString();
}
