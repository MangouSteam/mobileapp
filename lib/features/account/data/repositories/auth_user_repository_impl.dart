import 'package:firebase_auth/firebase_auth.dart';
import 'package:ltqd/core/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:ltqd/core/network_info.dart';
import 'package:ltqd/core/usecases/usecase.dart';
import 'package:ltqd/features/account/data/datasources/auth_user_remote_data_source.dart';
import 'package:ltqd/features/account/domain/entities/user.dart';
import 'package:ltqd/features/account/domain/repositories/auth_user_repository.dart';
import 'package:ltqd/features/account/domain/usecases/register_user.dart';
import 'package:ltqd/features/account/domain/usecases/signin_user.dart';
import 'package:ltqd/features/account/presentation/bloc/bloc/auth_bloc.dart';

class AuthUserRepositoryImpl implements AuthUserRepository {
  final AuthUserRemoteDataSource remoteDataSource;
  final NetworkInfo networkInfo;

  AuthUserRepositoryImpl({
    required this.remoteDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, LTQDUser>> signIn(
      {required AuthProvider provider, required SignInData data}) async {
    final eitherFailureOrUserCredential =
        await remoteDataSource.signIn(provider: provider, data: data);
    return eitherFailureOrUserCredential.fold((failure) => Left(AuthFailure()),
        (userCredential) {
      final LTQDUser ltdqUser = LTQDUser(
        displayName: userCredential.user!.displayName!,
        email: userCredential.user!.email!,
      );
      return Right(ltdqUser);
    });
  }

  @override
  Future<Either<Failure, LTQDUser>> register(
      {required RegisterData data}) async {
    final eitherFailureOrUserCredential =
        await remoteDataSource.register(data: data);
    return eitherFailureOrUserCredential.fold((failure) => Left(AuthFailure()),
        (userCredential) {
      final LTQDUser ltdqUser = LTQDUser(
        displayName: userCredential.user!.displayName!,
        email: userCredential.user!.email!,
      );
      return Right(ltdqUser);
    });
  }

  @override
  Stream<LTQDUser?> getAuthUserStream() {
    return remoteDataSource.getAuthUserStream().map((event) {
      if (event is User) {
        return LTQDUser(displayName: event.displayName!, email: event.email!);
      }
      return null;
    });
  }

  @override
  Future<void> signOut(NoParams noParams) {
    return remoteDataSource.signOut();
  }
}
