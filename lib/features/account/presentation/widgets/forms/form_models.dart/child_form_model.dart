class ChildFormModel {
  String? firstName;
  String? gender;

  ChildFormModel({this.firstName, this.gender});

  @override
  String toString() {
    return "ChildFormModel : firstName $firstName, gender $gender";
  }
}
