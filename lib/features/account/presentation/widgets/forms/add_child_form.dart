import 'package:flutter/material.dart';
import 'package:ltqd/core/injection_container.dart';
import 'package:ltqd/core/option.dart';
import 'package:ltqd/features/account/presentation/bloc/child_form/child_form_bloc.dart';
import 'package:ltqd/features/account/presentation/widgets/forms/form_models.dart/child_form_model.dart';
import 'package:ltqd/features/shared/presentation/widgets/app_bar.dart';
import 'package:ltqd/features/shared/presentation/widgets/forms/options_field.dart';
import 'package:ltqd/features/tracking/domain/object_values/gender.dart';

class AddChildForm extends StatefulWidget {
  AddChildForm({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return AddChildFormState();
  }
}

class AddChildFormState extends State<AddChildForm> {
  final _formKey = GlobalKey<FormState>();

  final _formModel = new ChildFormModel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: LTQDAppBar(name: 'Add child'),
      body: Container(
        margin: EdgeInsets.only(top: 30),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TextFormField(
                onSaved: (value) {
                  _formModel.firstName = value!;
                },
                obscureText: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Prénom de l'enfant",
                ),
              ),
              OptionsField(
                title: 'Genre :',
                collection: [
                  Option(
                    label: Gender.boy,
                    asset: 'asset/images/sleepingboy.png',
                  ),
                  Option(
                    label: Gender.girl,
                    asset: 'asset/images/sleepinggirl.png',
                  )
                ],
                onSaved: () {},
              ),
              ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    _formKey.currentState!.save();
                    _formModel.gender = Gender.boy;
                    sl<ChildFormBloc>()
                        .add(ChildFormSubmitted(childFormModel: _formModel));
                  }
                },
                child: Text('Je valide'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
