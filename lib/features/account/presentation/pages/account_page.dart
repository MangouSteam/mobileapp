import 'package:flutter/material.dart';
import 'package:ltqd/constants/colors.dart';

class AccountPage extends StatelessWidget {
  AccountPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
          Container(
            margin: EdgeInsets.symmetric(vertical: 20),
            child: Column(
              children: [
                CircleAvatar(
                  radius: 40,
                ),
                Text(
                  'Martine',
                  style: TextStyle(
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
                Text(
                  'martine.pelu@gmail.com',
                  style: TextStyle(
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.bold,
                    fontSize: 14,
                  ),
                ),
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: AppColors.backgroundColor,
            ),
            height: 60,
            width: double.infinity,
            child: Center(child: Text('Changer le mot de passe')),
          ),
          Text('Les enfants'),
          GestureDetector(
            onTap: () {},
            child: Container(
              decoration: BoxDecoration(
                color: AppColors.backgroundColor,
              ),
              height: 60,
              width: double.infinity,
              child: Center(child: Text('Ajouter un enfant')),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: AppColors.backgroundColor,
            ),
            height: 60,
            width: double.infinity,
            child: Center(child: Text('Contacter le support')),
          ),
          TextButton(
            onPressed: () {},
            child: Text('Se déconnecter'),
          )
        ]),
      ),
    );
  }
}
