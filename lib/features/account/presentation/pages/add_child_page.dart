import 'package:flutter/material.dart';
import 'package:ltqd/features/account/presentation/widgets/forms/add_child_form.dart';

class AddChildPage extends StatelessWidget {
  const AddChildPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: AddChildForm(),
    );
  }
}
