part of 'child_form_bloc.dart';

abstract class ChildFormState extends Equatable {
  const ChildFormState();

  @override
  List<Object> get props => [];
}

class ChildFormInitial extends ChildFormState {}

class ChildFormSubmitInProgress extends ChildFormState {}

class ChildFormSubmitFailed extends ChildFormState {}

class ChildFormSubmitSuccess extends ChildFormState {}
