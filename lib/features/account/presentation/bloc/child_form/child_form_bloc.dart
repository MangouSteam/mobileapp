import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:ltqd/features/account/presentation/widgets/forms/form_models.dart/child_form_model.dart';
import 'package:ltqd/features/tracking/domain/entities/children.dart';
import 'package:ltqd/features/tracking/domain/object_values/sleep_timer.dart';
import 'package:ltqd/features/tracking/domain/usecases/add_new_child.dart';

part 'child_form_event.dart';
part 'child_form_state.dart';

class ChildFormBloc extends Bloc<ChildFormEvent, ChildFormState> {
  final AddNewChild addNewChild;

  ChildFormBloc({required this.addNewChild}) : super(ChildFormInitial()) {
    on<ChildFormEvent>((event, emit) async {
      if (event is ChildFormSubmitted) {
        emit(ChildFormSubmitInProgress());
        final child = _transformFormModelToEntity(event.childFormModel);
        final eitherSuccessOrFailure =
            await addNewChild(AddNewChildParams(child: child));
        eitherSuccessOrFailure.fold((error) {
          emit(ChildFormSubmitFailed());
        }, (addNewChild) {
          emit(ChildFormSubmitSuccess());
        });
      } else if (event is ChildFormReset) {
        emit(ChildFormInitial());
      }
    });
  }

  Children _transformFormModelToEntity(ChildFormModel childFormModel) {
    print(childFormModel);
    return Children(
        firstName: childFormModel.firstName!,
        gender: childFormModel.gender!,
        active: false,
        sleepTimer: SleepTimer(
          currentlyTicking: false,
          startTime: DateTime.now(),
        ),
        id: '');
  }
}
