part of 'child_form_bloc.dart';

abstract class ChildFormEvent extends Equatable {
  const ChildFormEvent();

  @override
  List<Object> get props => [];
}

class ChildFormSubmitted extends ChildFormEvent {
  final ChildFormModel childFormModel;
  ChildFormSubmitted({required this.childFormModel});
}

class ChildFormReset extends ChildFormEvent {}
