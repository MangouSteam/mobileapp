part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}

class Listening extends AuthEvent {}

class Unauthenticate extends AuthEvent {}

class Authenticating extends AuthEvent {
  final AuthProvider provider;
  final String? password;
  final String? email;

  Authenticating({
    required this.provider,
    this.password,
    this.email,
  });
}

class Registering extends AuthEvent {
  final AuthProvider provider;
  final String? password;
  final String? email;

  Registering({
    required this.provider,
    this.password,
    this.email,
  });
}

class AuthenticateFailed extends AuthEvent {}

class AuthenticateSuccess extends AuthEvent {
  final LTQDUser user;

  AuthenticateSuccess(this.user);
}

enum AuthProvider { google, facebook, apple, emailPassword, testOk, testPasOk }
