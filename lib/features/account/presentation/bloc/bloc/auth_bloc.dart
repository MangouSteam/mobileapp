import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:ltqd/core/failure.dart';
import 'package:ltqd/core/usecases/usecase.dart';
import 'package:ltqd/features/account/domain/entities/user.dart';
import 'package:ltqd/features/account/domain/usecases/get_auth_stream.dart';
import 'package:ltqd/features/account/domain/usecases/register_user.dart';
import 'package:ltqd/features/account/domain/usecases/signin_user.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final SignInUser signInUser;
  final RegisterUser registerUser;
  final GetAuthUserStream getAuthUserStream;

  AuthBloc(
      {required this.signInUser,
      required this.registerUser,
      required this.getAuthUserStream})
      : super(Unauthenticated()) {
    on<Listening>((event, emit) async {
      getAuthUserStream(NoParams()).listen((LTQDUser? user) {
        if (user == null) {
          Authenticated(user!);
        } else {
          Unauthenticated();
        }
      });
    });
    on<Authenticating>((event, emit) async {
      final authOrFailure = await _authenticateUser(
          provider: event.provider,
          email: event.email,
          password: event.password);
      authOrFailure.fold((failure) {
        add(AuthenticateFailed());
      }, (user) {
        add(AuthenticateSuccess(user));
      });
    });
    on<AuthenticateFailed>((event, emit) => emit(Unauthenticated()));
    on<AuthenticateSuccess>((event, emit) => emit(Authenticated(event.user)));
    on<Registering>((event, emit) async {
      final failureOrRegister =
          await _registerUser(email: event.email!, password: event.password!);
      failureOrRegister.fold((failure) {
        add(AuthenticateFailed());
      }, (user) {
        add(AuthenticateSuccess(user));
      });
    });
    on<Unauthenticate>((event, emit) {
      emit(Unauthenticated());
    });
  }

  Future<Either<Failure, LTQDUser>> _registerUser(
      {required String email, required String password}) async {
    return registerUser(RegisterUserParams(
        data: RegisterData(email: email, password: password)));
  }

  Future<Either<Failure, LTQDUser>> _authenticateUser(
      {required provider, email, password}) {
    return signInUser(
      SignInUserParams(
        provider: provider,
        data: SignInData(email: email, password: password),
      ),
    );
  }
}
