part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

class Unauthenticated extends AuthState {}

class Authenticated extends AuthState {
  final LTQDUser user;

  Authenticated(this.user);
}
