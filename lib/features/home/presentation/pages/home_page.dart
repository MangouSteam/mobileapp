import 'package:flutter/material.dart';
import 'package:ltqd/constants/colors.dart';
import 'package:ltqd/features/home/data/datasources/factsheets_fixtures.dart';
import 'package:ltqd/features/home/presentation/widgets/factsheet_item.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);

  final List _factSheets = FactsheetFixtures().all();

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(20.0),
        color: AppColors.backgroundColor,
        child: SizedBox(
          width: double.infinity,
          height: double.infinity,
          child: ListView(children: [
            Text('Le sommeil de Gustave',
                style: Theme.of(context).textTheme.headline1),
            Text('Fiches pratiques',
                style: Theme.of(context).textTheme.headline2),
            SizedBox(
                width: double.infinity,
                height: 900,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: _factSheets.length,
                  itemBuilder: (context, index) =>
                      FactsheetItem(factsheet: _factSheets[index]),
                ))
          ]),
        ));
  }
}
