import 'package:flutter/material.dart';
import 'package:ltqd/constants/colors.dart';
import 'package:ltqd/features/home/data/models/factsheet.dart';
import 'package:ltqd/features/shared/presentation/pages/detail_base_page.dart';

class ContentDetailsPage extends StatelessWidget {
  final Factsheet factsheet;
  ContentDetailsPage({Key? key, required this.factsheet}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DetailBasePage(
        injected: Container(
            padding: EdgeInsets.all(20.0),
            color: AppColors.backgroundColor,
            child: SizedBox(
              width: double.infinity,
              height: double.infinity,
              child: ListView(children: [
                Text(this.factsheet.title,
                    style: Theme.of(context).textTheme.headline1),
                Text(this.factsheet.body,
                    style: Theme.of(context).textTheme.bodyText1),
              ]),
            )));
  }
}
