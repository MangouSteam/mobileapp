import 'package:flutter/material.dart';
import 'package:ltqd/constants/colors.dart';
import 'package:ltqd/features/home/data/models/factsheet.dart';
import 'package:ltqd/features/home/presentation/pages/content_details_page.dart';

class FactsheetItem extends StatelessWidget {
  final Factsheet factsheet;

  const FactsheetItem({Key? key, required this.factsheet}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ContentDetailsPage(factsheet: this.factsheet),
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.only(right: 16),
        child: Column(
          children: [
            Container(
              width: 160,
              height: 110,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(this.factsheet.imageUri),
                  fit: BoxFit.cover,
                ),
                color: AppColors.orange,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
              ),
            ),
            Container(
              width: 160,
              height: 70,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(30),
                    bottomRight: Radius.circular(30),
                  )),
              child: SizedBox(
                child: Center(
                  child: Text(
                    this.factsheet.title,
                    textAlign: TextAlign.center,
                  ),
                ),
                width: 100,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
