import 'package:ltqd/features/home/data/models/factsheet.dart';

class FactsheetFixtures {
  static Factsheet _factsheet1 = new Factsheet(
    "Habiller bébé selon la température",
    "",
    "https://images.unsplash.com/photo-1604917621956-10dfa7cce2e7?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1212&q=80",
  );
  static Factsheet _factsheet2 = new Factsheet(
      "Les heures de sommeil selon l'âge de l'enfant",
      "",
      "https://images.unsplash.com/photo-1546015720-b8b30df5aa27?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2250&q=80");
  static Factsheet _factsheet3 = new Factsheet(
      "Les causes de réveil de l'enfant",
      "",
      "https://images.unsplash.com/photo-1522771930-78848d9293e8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTB8fGJhYnl8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60");

  List<Factsheet> _collection = [
    _factsheet1,
    _factsheet2,
    _factsheet3,
  ];

  List<Factsheet> all() {
    return _collection;
  }
}
