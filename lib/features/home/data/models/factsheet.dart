class Factsheet {
  final String title;
  final String body;
  final String imageUri;

  Factsheet(this.title, this.body, this.imageUri);
}
