import 'package:ltqd/core/option.dart';

class OptionsData {
  static Option _option1 = Option(
    label: 'cool',
    asset: 'assets/images/sleepinggirl.png',
  );

  static Option _option2 = Option(
    label: 'cool',
    asset: 'assets/images/sleepinggirl.png',
  );
  static Option _option3 = Option(
    label: 'cool',
    asset: 'assets/images/sleepinggirl.png',
  );

  static Option _option4 = Option(
    label: 'cool',
    asset: 'assets/images/sleepinggirl.png',
  );

  List<Option> _collection = [
    _option1,
    _option2,
    _option3,
    _option4,
  ];

  List<Option> all() {
    return _collection;
  }
}
