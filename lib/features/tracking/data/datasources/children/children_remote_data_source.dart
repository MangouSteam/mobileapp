import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ltqd/core/injection_container.dart';
import 'package:ltqd/features/tracking/data/models/children.dart';

abstract class ChildrenRemoteDataSource {
  Future<ChildrenModel> getChildById(id);

  Future<List<ChildrenModel>> getChildren();

  Stream<QuerySnapshot<Object?>> getChildrenSubscription();

  Future<void> setSleepTimer({childrenId, sleepTimer});

  Future<DocumentReference> addNewChild(data);
}

final CollectionReference childrenRef = sl<FirebaseFirestore>()
    .collection('children')
    .withConverter<ChildrenModel>(
      fromFirestore: (snapshot, _) => ChildrenModel.fromJson(snapshot.data()!),
      toFirestore: (children, _) => children.toJson(),
    );

class ChildrenRemoteDataSourceImpl implements ChildrenRemoteDataSource {
  @override
  Future<ChildrenModel> getChildById(id) async {
    return await childrenRef
        .doc(id)
        .get()
        .then((snapshot) => snapshot.data() as ChildrenModel);
  }

  @override
  Future<List<ChildrenModel>> getChildren() async {
    final userId = 'lMmeAUPaorS9AMPB5ZwUAtnOmkI3';
    return await childrenRef
        .where('userAccess', arrayContains: userId)
        .limit(7)
        .get()
        .then((querySnapshot) => querySnapshot.docs.map((doc) {
              return (doc.data() as ChildrenModel);
            }).toList());
  }

  Stream<QuerySnapshot<Object?>> getChildrenSubscription() {
    final userId = 'lMmeAUPaorS9AMPB5ZwUAtnOmkI3';
    return childrenRef
        .where('userAccess', arrayContains: userId)
        .limit(7)
        .snapshots();
  }

  @override
  Future<void> setSleepTimer({childrenId, sleepTimer}) {
    return childrenRef.doc(childrenId).update({"sleepTimer": sleepTimer});
  }

  @override
  Future<DocumentReference> addNewChild(data) async {
    print(data);
    return childrenRef.add(data);
  }
}
