import 'package:ltqd/features/tracking/data/models/children.dart';
import 'package:ltqd/features/tracking/domain/object_values/gender.dart';

class ChildrenData {
  static ChildrenModel _children1 = new ChildrenModel(
    id: 'a',
    firstName: "Gustave",
    gender: Gender.boy,
    initials: 'GU',
  );
  static ChildrenModel _children2 = new ChildrenModel(
    id: 'b',
    firstName: "Alice",
    gender: Gender.girl,
    initials: 'AL',
  );
  static ChildrenModel _children3 = new ChildrenModel(
    id: 'c',
    firstName: "Martin",
    gender: Gender.boy,
    initials: 'MA',
  );
  static ChildrenModel _children4 = new ChildrenModel(
    id: 'd',
    firstName: "Philippe",
    gender: Gender.boy,
    initials: 'PA',
  );

  List<ChildrenModel> _collection = [
    _children1,
    _children2,
    _children3,
    _children4
  ];

  List<ChildrenModel> all() {
    return _collection;
  }
}
