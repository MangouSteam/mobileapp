import 'dart:convert';

import 'package:ltqd/core/exception.dart';
import 'package:ltqd/features/tracking/data/models/children.dart';
import 'package:ltqd/features/tracking/data/models/collections/children_collection.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class ChildrenLocalDataSource {
  Future<ChildrenModel> getLastChild();

  Future<List<ChildrenModel>> getLastChildren();

  Future<void> cacheChild(ChildrenModel childToCache);

  Future<void> cacheChildren(List<ChildrenModel> childrenToCache);
}

const CACHED_CHILDREN = 'CACHED_CHILDREN';
const CACHED_CHILD = 'CACHED_CHILD';

class ChildrenLocalDataSourceImpl implements ChildrenLocalDataSource {
  final SharedPreferences sharedPreferences;

  ChildrenLocalDataSourceImpl({required this.sharedPreferences});

  @override
  Future<void> cacheChild(ChildrenModel childToCache) {
    throw UnimplementedError();
  }

  @override
  Future<void> cacheChildren(List<ChildrenModel> children) {
    final childrenTocache = ChildrenModelCollection(children);
    return sharedPreferences.setString(
        CACHED_CHILDREN,
        json.encode(
          childrenTocache.toJson(),
        ));
  }

  @override
  Future<ChildrenModel> getLastChild() {
    throw UnimplementedError();
  }

  @override
  Future<List<ChildrenModel>> getLastChildren() {
    final jsonString = sharedPreferences.getString(CACHED_CHILDREN);
    if (jsonString != null) {
      return Future.value(
          ChildrenModelCollection.fromJson(json.decode(jsonString)));
    } else {
      throw CacheException();
    }
  }
}
