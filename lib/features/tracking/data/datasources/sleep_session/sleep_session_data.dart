import 'package:ltqd/features/tracking/data/models/sleep_session.dart';

class SleepSessionData {
  static final _data1 = SleepSessionModel(
    childrenId: 'a',
    endTime: DateTime.now(),
    startTime: DateTime.now(),
  );

  static final _data2 = SleepSessionModel(
    childrenId: 'a',
    endTime: DateTime.now(),
    startTime: DateTime.now(),
  );

  static final _data3 = SleepSessionModel(
    childrenId: 'a',
    endTime: DateTime.now(),
    startTime: DateTime.now(),
  );

  static final _data4 = SleepSessionModel(
    childrenId: 'a',
    endTime: DateTime.now(),
    startTime: DateTime.now(),
  );

  static final _data5 = SleepSessionModel(
    childrenId: 'a',
    endTime: DateTime.now(),
    startTime: DateTime.now(),
  );

  List<SleepSessionModel> _collection = [
    _data1,
    _data2,
    _data3,
    _data4,
    _data5,
  ];

  List<SleepSessionModel> all() {
    return _collection;
  }
}
