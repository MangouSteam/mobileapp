import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ltqd/core/injection_container.dart';
import 'package:ltqd/features/tracking/data/models/sleep_session.dart';

abstract class SleepSessionRemoteDataSource {
  Future<SleepSessionModel> getSleepSessionById(id);

  Future<List<SleepSessionModel>> getSleepSessions({childrenId});

  Future<String> setSleepSession(SleepSessionModel data,
      [String sleepSessionId]);
}

final CollectionReference sleepSessionRef = sl<FirebaseFirestore>()
    .collection('sleep_sessions')
    .withConverter<SleepSessionModel>(
      fromFirestore: (snapshot, _) =>
          SleepSessionModel.fromJson(snapshot.data()!),
      toFirestore: (children, _) => children.toJson(),
    );

/* @Injectable(as: SleepSessionRemoteDataSource)
 */
class SleepSessionRemoteDataSourceImpl implements SleepSessionRemoteDataSource {
  @override
  Future<List<SleepSessionModel>> getSleepSessions({childrenId}) async {
    return await sleepSessionRef
        .where('childrenId', isEqualTo: childrenId)
        .limit(31)
        .get()
        .then((snapshot) => snapshot.docs as List<SleepSessionModel>);
  }

  @override
  Future<SleepSessionModel> getSleepSessionById(id) async {
    return await sleepSessionRef
        .doc(id)
        .get()
        .then((snapshot) => snapshot.data() as SleepSessionModel);
  }

  @override
  Future<String> setSleepSession(SleepSessionModel data,
      [String sleepSessionId = '']) async {
    if (sleepSessionId.length > 0) {
      await sleepSessionRef.doc(sleepSessionId).update(data.toJson());
      return sleepSessionId;
    }
    return sleepSessionRef.add(data).then((docRef) => docRef.id);
  }
}
