import 'dart:convert';

import 'package:ltqd/core/exception.dart';
import 'package:ltqd/features/tracking/data/models/collections/sleep_session_collection.dart';
import 'package:ltqd/features/tracking/data/models/sleep_session.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class SleepSessionLocalDataSource {
  Future<SleepSessionModel> getLastSleepSession();

  Future<List<SleepSessionModel>> getLastSleepSessions();

  Future<void> cacheSleepSession(SleepSessionModel sleepSessionToCache);

  Future<void> cacheSleepSessions(List<SleepSessionModel> sleepSessionToCache);
}

const CACHED_SLEEP_SESSIONS = 'CACHED_CHILDREN';
const CACHED_SLEEP_SESSION = 'CACHED_CHILD';

class SleepSessionLocalDataSourceImpl implements SleepSessionLocalDataSource {
  final SharedPreferences sharedPreferences;

  SleepSessionLocalDataSourceImpl({required this.sharedPreferences});

  @override
  Future<void> cacheSleepSession(SleepSessionModel sleepSessionToCache) {
    return sharedPreferences.setString(
        CACHED_SLEEP_SESSION,
        json.encode(
          sleepSessionToCache.toJson(),
        ));
  }

  @override
  Future<void> cacheSleepSessions(List<SleepSessionModel> sleepSessions) {
    final sleepSessionsToCache = SleepSessionModelCollection(sleepSessions);
    return sharedPreferences.setString(
        CACHED_SLEEP_SESSIONS,
        json.encode(
          sleepSessionsToCache.toJson(),
        ));
  }

  @override
  Future<SleepSessionModel> getLastSleepSession() {
    throw UnimplementedError();
  }

  @override
  Future<List<SleepSessionModel>> getLastSleepSessions() {
    final jsonString = sharedPreferences.getString(CACHED_SLEEP_SESSIONS);
    if (jsonString != null) {
      return Future.value(
          SleepSessionModelCollection.fromJson(json.decode(jsonString)));
    } else {
      throw CacheException();
    }
  }
}
