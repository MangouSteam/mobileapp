import 'package:dartz/dartz.dart';
import 'package:ltqd/core/exception.dart';
import 'package:ltqd/core/failure.dart';
import 'package:ltqd/core/network_info.dart';
import 'package:ltqd/features/tracking/data/datasources/children/children_local_data_source.dart';
import 'package:ltqd/features/tracking/data/datasources/children/children_remote_data_source.dart';
import 'package:ltqd/features/tracking/data/models/children.dart';
import 'package:ltqd/features/tracking/data/models/collections/children_collection.dart';
import 'package:ltqd/features/tracking/domain/entities/children.dart';
import 'package:ltqd/features/tracking/domain/object_values/sleep_timer.dart';
import 'package:ltqd/features/tracking/domain/repositories/children_repository.dart';

class ChildrenRepositoryImpl implements ChildrenRepository {
  final ChildrenRemoteDataSource remoteDataSource;
  final ChildrenLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  ChildrenRepositoryImpl({
    required this.remoteDataSource,
    required this.localDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, Children>> getChildById({id = id}) async {
    if (await networkInfo.isConnected) {
      try {
        final remoteChildren = await remoteDataSource.getChildById(id);
        localDataSource.cacheChild(remoteChildren);
        return Right(remoteChildren);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localChildren = await localDataSource.getLastChild();
        return Right(localChildren);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, List<Children>>> getChildren() async {
    if (await networkInfo.isConnected) {
      try {
        final remoteChildren = await remoteDataSource.getChildren();
        final List<Children> collection =
            ChildrenModelCollection(remoteChildren).toEntities();
        localDataSource.cacheChildren(remoteChildren);
        return Right(collection);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localChildren = await localDataSource.getLastChildren();
        return Right(localChildren);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Stream<List<Children>> getChildrenStream() {
    return remoteDataSource.getChildrenSubscription().map((snapshot) {
      return snapshot.docs
          .map((doc) => (doc.data() as ChildrenModel).toEntity() as Children)
          .toList();
    });
  }

  @override
  Future<Either<Failure, bool>> startSleepTimer(
      {required DateTime startTime, required String childrenId}) async {
    try {
      final sleepTimer = SleepTimer(
        startTime: startTime,
        currentlyTicking: true,
      );
      await remoteDataSource.setSleepTimer(
        childrenId: childrenId,
        sleepTimer: ChildrenModel.sleepTimerToJson(sleepTimer),
      );
      return Right(true);
    } on Exception {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> endSleepTimer(
      {required String childrenId}) async {
    try {
      final sleepTimer = SleepTimer(
        startTime: DateTime.now(),
        currentlyTicking: false,
      );
      await remoteDataSource.setSleepTimer(
        childrenId: childrenId,
        sleepTimer: ChildrenModel.sleepTimerToJson(sleepTimer),
      );
      return Right(true);
    } on Exception {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> addNewChild({required Children child}) async {
    try {
      final childToBeAdded = child.toModel();
      await remoteDataSource.addNewChild(childToBeAdded);
      return Right(true);
    } on ServerException {
      return Left(ServerFailure());
    }
  }
}
