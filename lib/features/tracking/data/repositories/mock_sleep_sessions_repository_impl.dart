import 'package:ltqd/features/tracking/domain/repositories/sleep_sessions_repository.dart';
import 'package:mocktail/mocktail.dart';

class MockSleepSessionsRepositoryImpl extends Mock
    implements SleepSessionsRepository {}
