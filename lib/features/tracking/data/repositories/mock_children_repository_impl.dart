import 'package:ltqd/features/tracking/data/repositories/children_repository_impl.dart';
import 'package:mocktail/mocktail.dart';

class MockChildrenRepositoryImpl extends Mock
    implements ChildrenRepositoryImpl {}
