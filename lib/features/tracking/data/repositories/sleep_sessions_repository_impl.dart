import 'package:dartz/dartz.dart';
import 'package:ltqd/core/exception.dart';
import 'package:ltqd/core/failure.dart';
import 'package:ltqd/core/network_info.dart';
import 'package:ltqd/features/tracking/data/datasources/sleep_session/sleep_session_local_data.dart';
import 'package:ltqd/features/tracking/data/datasources/sleep_session/sleep_session_remote_data.dart';
import 'package:ltqd/features/tracking/data/models/collections/sleep_session_collection.dart';
import 'package:ltqd/features/tracking/data/models/sleep_session.dart';
import 'package:ltqd/features/tracking/domain/entities/sleep_session.dart';
import 'package:ltqd/features/tracking/domain/repositories/sleep_sessions_repository.dart';

class SleepSessionsRepositoryImpl implements SleepSessionsRepository {
  final SleepSessionRemoteDataSource remoteDataSource;
  final SleepSessionLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  SleepSessionsRepositoryImpl({
    required this.remoteDataSource,
    required this.localDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, SleepSession>> getSleepSessionById({id = id}) async {
    if (await networkInfo.isConnected) {
      try {
        final remoteSleepSession =
            await remoteDataSource.getSleepSessionById(id);
        //localDataSource.cacheSleepSession(remoteSleepSession);
        return Right(remoteSleepSession);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localChildren = await localDataSource.getLastSleepSession();
        return Right(localChildren);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, List<SleepSession>>> getSleepSessions(
      {required childrenId}) async {
    if (await networkInfo.isConnected) {
      try {
        final remoteSleepSession = await remoteDataSource.getSleepSessions();
        final List<SleepSession> collection =
            SleepSessionModelCollection(remoteSleepSession).toEntities();
        //localDataSource.cacheSleepSessions(remoteSleepSession);
        return Right(collection);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localChildren = await localDataSource.getLastSleepSessions();
        return Right(localChildren);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, String>> startSleepSession(
      {required childrenId, required startTime}) async {
    try {
      SleepSessionModel sleepSession = SleepSessionModel(
        startTime: startTime,
        endTime: null,
        childrenId: childrenId,
      );
      final sleepSessionId =
          await remoteDataSource.setSleepSession(sleepSession);
      //localDataSource.cacheSleepSession(sleepSession);
      return Right(sleepSessionId);
    } on ServerException {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, String>> endSleepSession(
      {required DateTime endTime, required String sleepSessionId}) async {
    try {
      SleepSessionModel sleepSession = SleepSessionModel(
        endTime: endTime,
      );
      await remoteDataSource.setSleepSession(sleepSession, sleepSessionId);
      //localDataSource.cacheSleepSession(sleepSession);
      return Right(sleepSessionId);
    } on ServerException {
      return Left(ServerFailure());
    }
  }
}
