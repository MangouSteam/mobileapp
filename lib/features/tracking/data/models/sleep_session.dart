import 'package:ltqd/features/tracking/domain/entities/sleep_session.dart';

class SleepSessionModel extends SleepSession {
  final String? childrenId;
  final DateTime? startTime;
  final DateTime? endTime;

  SleepSessionModel({
    this.childrenId,
    this.startTime,
    this.endTime,
  }) : super(
          childrenId: childrenId,
          startTime: startTime,
          endTime: endTime,
        );

  Map<String, dynamic> toJson() {
    return {
      'childrenId': childrenId,
      'startTime': startTime,
      'endTime': endTime,
    };
  }

  factory SleepSessionModel.fromJson(Map<String, dynamic> json) {
    return SleepSessionModel(
      childrenId: json['childrenId'],
      startTime: json['startTime'],
      endTime: json['endTime'],
    );
  }
}
