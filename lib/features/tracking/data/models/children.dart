import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ltqd/features/tracking/domain/entities/children.dart';
import 'package:ltqd/features/tracking/domain/object_values/sleep_timer.dart';

class ChildrenModel extends Children {
  final String firstName;
  final String gender;
  final String initials;
  final String id;
  final SleepTimer? sleepTimer;

  ChildrenModel(
      {required this.firstName,
      required this.gender,
      required this.id,
      this.sleepTimer,
      required this.initials})
      : super(
            firstName: firstName,
            gender: gender,
            id: id,
            initials: initials,
            sleepTimer: sleepTimer,
            active: false);

  Map<String, dynamic> toJson() {
    return {
      'firstName': firstName,
      'gender': gender,
      'id': id,
      'initials': initials,
      'sleepTimer': ChildrenModel.sleepTimerToJson(sleepTimer!)
    };
  }

  ChildrenModel.fromJson(Map<String, dynamic> json)
      : this(
          firstName: json['firstName']! as String,
          gender: json['gender']! as String,
          id: json['id']! as String,
          initials: json['initials']! as String,
          sleepTimer: json.containsKey('sleepTimer')
              ? SleepTimer(
                  startTime: DateTime.fromMicrosecondsSinceEpoch(
                      (json['sleepTimer']['startTime']! as Timestamp)
                          .microsecondsSinceEpoch
                          .toInt()),
                  currentlyTicking:
                      json['sleepTimer']['currentlyTicking']! as bool,
                )
              : SleepTimer(
                  startTime: DateTime.now(),
                  currentlyTicking: false,
                ),
        );

  toEntity() {
    return Children(
      initials: initials,
      active: active,
      firstName: firstName,
      gender: gender,
      id: id,
      sleepTimer: sleepTimer,
    );
  }

  static Map<String, dynamic> sleepTimerToJson(SleepTimer sleepTimer) {
    return {
      'currentlyTicking': sleepTimer.currentlyTicking,
      'startTime': sleepTimer.startTime
    };
  }

  static initialsFromFirstName(String firstName) {
    return firstName.substring(0, 2).toUpperCase();
  }
}
