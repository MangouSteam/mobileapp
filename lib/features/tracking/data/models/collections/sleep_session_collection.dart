import 'package:ltqd/features/tracking/data/models/sleep_session.dart';
import 'package:ltqd/features/tracking/domain/entities/sleep_session.dart';

class SleepSessionModelCollection {
  final List<SleepSessionModel> _collection;

  SleepSessionModelCollection(this._collection);

  List<Map<String, dynamic>> toJson() {
    return _collection.map((x) {
      return x.toJson();
    }).toList();
  }

  List<SleepSession> toEntities() {
    return _collection.map((x) {
      return SleepSession(
        childrenId: x.childrenId,
        endTime: x.endTime,
        startTime: x.startTime,
      );
    }).toList();
  }

  static fromJson(List<Map<String, dynamic>> json) {
    return json.map((x) {
      return SleepSessionModel.fromJson(x);
    }).toList();
  }
}
