import 'package:ltqd/features/tracking/data/models/children.dart';
import 'package:ltqd/features/tracking/domain/entities/children.dart';

class ChildrenModelCollection {
  final List<ChildrenModel> _collection;

  ChildrenModelCollection(this._collection);

  List<Map<String, dynamic>> toJson() {
    return _collection.map((x) {
      return x.toJson();
    }).toList();
  }

  List<Children> toEntities() {
    return _collection.map((x) {
      return Children(
        firstName: x.firstName,
        active: x.active,
        gender: x.gender,
        id: x.id,
      );
    }).toList();
  }

  static fromJson(List<Map<String, dynamic>> json) {
    return json.map((x) {
      return ChildrenModel.fromJson(x);
    }).toList();
  }
}
