part of 'period_bloc.dart';

@immutable
abstract class PeriodEvent extends Equatable {}

class PeriodChanged extends PeriodEvent {
  final Period period;

  PeriodChanged({required this.period});

  @override
  List<Object?> get props => [period];
}
