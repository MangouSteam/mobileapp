import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:ltqd/features/tracking/domain/object_values/period.dart';
import 'package:meta/meta.dart';

part 'period_event.dart';
part 'period_state.dart';

final DEFAULT_PERIOD = Period(label: "Aujourd'hui");

class PeriodBloc extends Bloc<PeriodEvent, PeriodState> {
  PeriodBloc() : super(PeriodInitial(period: DEFAULT_PERIOD));

  @override
  Stream<PeriodState> mapEventToState(
    PeriodEvent event,
  ) async* {
    if (event is PeriodChanged) {
      yield PeriodActivateSuccess(period: event.period);
    }
  }
}
