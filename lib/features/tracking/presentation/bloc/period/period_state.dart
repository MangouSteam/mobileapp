part of 'period_bloc.dart';

@immutable
abstract class PeriodState extends Equatable {
  final Period period;

  PeriodState({required this.period});

  @override
  List<Object?> get props => [this.period.label];
}

class PeriodInitial extends PeriodState {
  final Period period;
  PeriodInitial({required this.period}) : super(period: period);
}

class PeriodActivateSuccess extends PeriodState {
  final Period period;

  PeriodActivateSuccess({required this.period}) : super(period: period);
}
