part of 'child_active_bloc.dart';

@immutable
abstract class ChildActiveState extends Equatable {}

class ChildActiveInitial extends ChildActiveState {
  @override
  List<Object?> get props => [];
}

class ChildActiveActivateSuccess extends ChildActiveState {
  final Children child;

  ChildActiveActivateSuccess({required this.child});

  @override
  List<Object?> get props => [child.id];
}
