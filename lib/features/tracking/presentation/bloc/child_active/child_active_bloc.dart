import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:ltqd/core/injection_container.dart';
import 'package:ltqd/features/tracking/domain/entities/children.dart';
import 'package:ltqd/features/tracking/presentation/bloc/tracking_mediator/tracking_mediator_bloc.dart';
import 'package:meta/meta.dart';

part 'child_active_event.dart';
part 'child_active_state.dart';

class ChildActiveBloc extends Bloc<ChildActiveEvent, ChildActiveState> {
  ChildActiveBloc() : super(ChildActiveInitial());

  @override
  Stream<ChildActiveState> mapEventToState(
    ChildActiveEvent event,
  ) async* {
    if (event is ChildActiveChildLoaded) {
      yield ChildActiveActivateSuccess(child: event.child);
      sl<TrackingMediatorBloc>()..add(MediatorChildActiveChanged());
    } else if (event is ChildActiveChildDeactivated) {
      yield ChildActiveInitial();
    }
  }
}
