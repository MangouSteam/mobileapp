part of 'child_active_bloc.dart';

@immutable
abstract class ChildActiveEvent extends Equatable {}

class ChildActiveChildLoaded extends ChildActiveEvent {
  final Children child;

  ChildActiveChildLoaded(this.child);

  @override
  List<Object?> get props => [child];
}

class ChildActiveChildDeactivated extends ChildActiveEvent {
  @override
  List<Object?> get props => [];
}
