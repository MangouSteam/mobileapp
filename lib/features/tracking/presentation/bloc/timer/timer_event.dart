part of 'timer_bloc.dart';

@immutable
abstract class TimerEvent extends Equatable {
  const TimerEvent();

  @override
  List<Object> get props => [];
}

class TimerStarted extends TimerEvent {
  final int duration;
  const TimerStarted({required this.duration});
}

class TimerInProgress extends TimerEvent {
  const TimerInProgress();
}

class TimerStopped extends TimerEvent {
  final int duration;
  TimerStopped({required this.duration});

  @override
  List<Object> get props => [duration];
}

class TimerPaused extends TimerEvent {
  final int duration;
  TimerPaused({required this.duration});

  @override
  List<Object> get props => [duration];
}

class TimerResumed extends TimerEvent {
  final int duration;
  TimerResumed({required this.duration});

  @override
  List<Object> get props => [duration];
}

class TimerReset extends TimerEvent {
  const TimerReset();
}

class TimerTicked extends TimerEvent {
  const TimerTicked({required this.duration});
  final int duration;

  @override
  List<Object> get props => [duration];
}
