import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:ltqd/core/injection_container.dart';
import 'package:ltqd/core/usecases/usecase.dart';
import 'package:ltqd/features/tracking/domain/entities/children.dart';
import 'package:ltqd/features/tracking/domain/usecases/get_children_stream.dart';
import 'package:ltqd/features/tracking/presentation/bloc/child_active/child_active_bloc.dart';
import 'package:meta/meta.dart';

part 'children_event.dart';
part 'children_state.dart';

const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String SERVER_FAILURE_MESSAGE = 'Server Failure';

class ChildrenBloc extends Bloc<ChildrenEvent, ChildrenState> {
  final GetChildrenStream getChildrenStream;

  StreamSubscription<List<Children>>? _childrenStreamSubscription;
  ChildrenBloc({required this.getChildrenStream}) : super(ChildrenInitial());

  @override
  Future<void> close() {
    _childrenStreamSubscription?.cancel();
    return super.close();
  }

  @override
  Stream<ChildrenState> mapEventToState(
    ChildrenEvent event,
  ) async* {
    if (event is ChildrenDataLoading) {
      yield* _mapIsLoadingToState();
    } else if (event is ChildrenChildActivated) {
      yield* _mapIsActivatedToState(event);
    } else if (event is ChildrenDataLoaded) {
      yield* _mapIsLoadedToState(event);
    }
  }

  void _doProcessActivation(List<Children> children) {
    Children activeChild = Children.setChildActive(children.first);
    children.removeAt(0);
    children.insert(0, activeChild);
    sl<ChildActiveBloc>()..add(ChildActiveChildLoaded(activeChild));
  }

  Stream<ChildrenState> _mapIsActivatedToState(
      ChildrenChildActivated event) async* {
    if (state is ChildrenLoadSuccess) {
      List<Children> children = state.children;
      String childIdToActivate = event.child.id;
      Children childToActivate =
          children.firstWhere((x) => x.id == childIdToActivate);
      Children activeChild = Children.setChildActive(childToActivate);
      int indexOf = children.indexOf(childToActivate);

      children.removeAt(indexOf);
      List<Children> childrenDeactivated =
          Children.setChildrenNotActive(children);
      childrenDeactivated.insert(indexOf, activeChild);

      sl<ChildActiveBloc>()..add(ChildActiveChildLoaded(activeChild));
      yield ChildrenLoadSuccess(children: childrenDeactivated);
    }
  }

  Stream<ChildrenState> _mapIsLoadedToState(event) async* {
    if ((event.children as List).isNotEmpty) {
      _doProcessActivation(event.children);
    }
    yield ChildrenLoadSuccess(children: event.children);
  }

  Stream<ChildrenState> _mapIsLoadingToState() async* {
    yield ChildrenLoadInProgress();
    _childrenStreamSubscription?.cancel();
    _childrenStreamSubscription =
        getChildrenStream(NoParams()).listen((onData) {
      add(ChildrenDataLoaded(children: onData));
    });
  }

  /* String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  } */
}
