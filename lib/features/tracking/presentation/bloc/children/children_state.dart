part of 'children_bloc.dart';

@immutable
abstract class ChildrenState extends Equatable {
  final List<Children> children;

  ChildrenState({required this.children});

  @override
  List<Object?> get props => [children];
}

class ChildrenInitial extends ChildrenState {
  ChildrenInitial() : super(children: []);
}

class ChildrenLoadInProgress extends ChildrenState {
  ChildrenLoadInProgress() : super(children: []);
}

class ChildrenLoadSuccess extends ChildrenState {
  final List<Children> children;
  ChildrenLoadSuccess({
    required this.children,
  }) : super(children: children);
}

class Error extends ChildrenState {
  final String message;
  Error(this.message) : super(children: []);
}
