part of 'children_bloc.dart';

@immutable
abstract class ChildrenEvent {}

class ChildrenDataLoading extends ChildrenEvent {}

class ChildrenDataLoaded extends ChildrenEvent {
  final List<Children> children;
  ChildrenDataLoaded({required this.children});
}

class ChildrenChildActivated extends ChildrenEvent {
  final Children child;
  ChildrenChildActivated(this.child);
}
