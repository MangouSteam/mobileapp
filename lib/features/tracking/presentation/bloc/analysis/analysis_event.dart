part of 'analysis_bloc.dart';

@immutable
abstract class AnalysisEvent {}

class AnalysisDataLoading extends AnalysisEvent {}

class AnalysisDataLoaded extends AnalysisEvent {}
