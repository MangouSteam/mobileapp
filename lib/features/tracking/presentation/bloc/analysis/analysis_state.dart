part of 'analysis_bloc.dart';

@immutable
abstract class AnalysisState {}

class AnalysisInitial extends AnalysisState {}

class AnalysisLoadInProgress extends AnalysisState {
  AnalysisLoadInProgress() : super();
}

class AnalysisLoadSuccess extends AnalysisState {
  final List<SleepSession> sleepSessions;
  AnalysisLoadSuccess({
    required this.sleepSessions,
  }) : super();
}

class Error extends AnalysisState {
  final String message;
  Error(this.message) : super();
}
