import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:ltqd/core/failure.dart';
import 'package:ltqd/features/tracking/domain/entities/sleep_session.dart';
import 'package:ltqd/features/tracking/domain/usecases/get_sleep_sessions.dart';
import 'package:meta/meta.dart';

part 'analysis_event.dart';
part 'analysis_state.dart';

const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';

class AnalysisBloc extends Bloc<AnalysisEvent, AnalysisState> {
  final GetSleepSessions getSleepSessions;
  AnalysisBloc({required this.getSleepSessions}) : super(AnalysisInitial());

  @override
  Stream<AnalysisState> mapEventToState(
    AnalysisEvent event,
  ) async* {
    if (event is AnalysisDataLoading) {
      final childrenId = ''; //sl<ChildActiveBloc>().state.child.id;
      final failureOrSleepSessions = await getSleepSessions(
          GetSleepSessionsParams(childrenId: childrenId));

      yield* failureOrSleepSessions.fold((failure) async* {
        yield Error(_mapFailureToMessage(failure));
      }, (sleepSessions) async* {
        //INIT A TIMER FOR EACH CHILDREN
        yield AnalysisLoadSuccess(sleepSessions: sleepSessions);
      });
    }
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }
}
