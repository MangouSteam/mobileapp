import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:ltqd/core/injection_container.dart';
import 'package:ltqd/features/tracking/domain/entities/children.dart';
import 'package:ltqd/features/tracking/domain/services/timer_service.dart';
import 'package:ltqd/features/tracking/domain/usecases/end_sleep_timer.dart';
import 'package:ltqd/features/tracking/domain/usecases/start_sleep_timer.dart';
import 'package:ltqd/features/tracking/presentation/bloc/child_active/child_active_bloc.dart';
import 'package:ltqd/features/tracking/presentation/bloc/timer/timer_bloc.dart';
import 'package:meta/meta.dart';

part 'tracking_mediator_event.dart';
part 'tracking_mediator_state.dart';

const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';

class TrackingMediatorBloc
    extends Bloc<TrackingMediatorEvent, TrackingMediatorState> {
  final StartSleepTimer startSleepTimer;
  final EndSleepTimer endSleepTimer;

  TrackingMediatorBloc({
    required this.startSleepTimer,
    required this.endSleepTimer,
  }) : super(TrackingMediatorInitial());

  @override
  Stream<TrackingMediatorState> mapEventToState(
    TrackingMediatorEvent event,
  ) async* {
    final ChildActiveState state = sl<ChildActiveBloc>().state;
    if (state is ChildActiveActivateSuccess) {
      if (event is MediatorTimerStarted) {
        print('startSleepTimer dataWritten');
        await startSleepTimer(StartSleepTimerParams(
            childrenId: state.child.id, startTime: DateTime.now()));
      } else if (event is MediatorTimerStopped) {
        final childrenId = state.child.id;
        print('endSleepTimer DataWritten');
        /*  await endSleepTimer(
          EndSleepTimerParams(
            childrenId: childrenId,
          ),
        ); */
      } else if (event is MediatorChildActiveChanged) {
        final activeChild = state.child;
        if (_activeChildHasCurrentSleepSession(activeChild)) {
          int _duration = TimerService.getDurationInSeconds(
              activeChild.sleepTimer!.startTime!);

          sl<TimerBloc>()..add(TimerStarted(duration: _duration));
        } else {
          sl<TimerBloc>()..add(TimerReset());
        }
      }
    }
  }

  bool _activeChildHasCurrentSleepSession(child) {
    return (child as Children).sleepTimer!.currentlyTicking;
  }

  /* String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  } */
}
