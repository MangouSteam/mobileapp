part of 'tracking_mediator_bloc.dart';

@immutable
abstract class TrackingMediatorState extends Equatable {}

class TrackingMediatorInitial extends TrackingMediatorState {
  @override
  List<Object?> get props => [];
}

class TrackingMediatorStarted extends TrackingMediatorState {
  final String sleepingSessionId;

  TrackingMediatorStarted({required this.sleepingSessionId});

  @override
  List<Object?> get props => [sleepingSessionId];
}
