part of 'tracking_mediator_bloc.dart';

@immutable
abstract class TrackingMediatorEvent {}

class MediatorTimerStarted extends TrackingMediatorEvent {}

class MediatorTimerStopped extends TrackingMediatorEvent {}

class MediatorSleepFormSent extends TrackingMediatorEvent {}

class MediatorChildActiveLoaded extends TrackingMediatorEvent {}

class MediatorChildActiveChanged extends TrackingMediatorEvent {}
