import 'package:flutter/material.dart';
import 'package:ltqd/features/tracking/presentation/widgets/components/children/child.dart';
import 'package:ltqd/features/tracking/presentation/widgets/components/children/children_selector.dart';

class SleepTrackingPage extends StatelessWidget {
  const SleepTrackingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(children: [
          ChildrenSelector(),
          Expanded(
              child: Container(
            padding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
            width: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/background.png'),
                    fit: BoxFit.cover)),
            child: Child(),
          )),
          //TrackingSelector()
        ]),
      ),
    );
  }
}
