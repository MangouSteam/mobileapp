import 'package:flutter/material.dart';
import 'package:ltqd/features/tracking/presentation/widgets/views/analysis_head.dart';
import 'package:ltqd/features/tracking/presentation/widgets/views/analysis_kpi.dart';

class AnalysisPage extends StatelessWidget {
  AnalysisPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Center(
      child: Column(children: [
        AnalysisHead(),
        AnalysisKpi(),
      ]),
    ));
  }
}
