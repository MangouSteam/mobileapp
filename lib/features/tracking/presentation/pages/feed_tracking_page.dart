import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ltqd/features/tracking/presentation/widgets/views/feed_tracking_view.dart';

class FeedTrackingPage extends StatelessWidget {
  const FeedTrackingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        /*  BlocProvider<TimerBloc>(
          lazy: false,
          create: (_) => TimerBloc(ticker: Ticker()),
        ), */
        //jBlocProvider<ChildrenBloc>(
        //j lazy: false,
        //create: (_) => ChildrenBloc(),
        //),
      ],
      child: FeedTrackingView(),
    );
  }
}
