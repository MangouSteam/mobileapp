import 'package:flutter/material.dart';
import 'package:ltqd/features/shared/presentation/pages/base_page.dart';

class TrackingSelector extends StatelessWidget {
  const TrackingSelector({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          TextButton(
            child: Text('SleepTracking'),
            onPressed: () => {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => BasePage(
                            index: 1,
                          )))
            },
          ),
          TextButton(
            child: Text('FeedTracking'),
            onPressed: () => {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => BasePage(
                            index: 1,
                          )))
            },
          ),
        ],
      ),
    );
  }
}
