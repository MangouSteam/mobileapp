import 'package:flutter/material.dart';
import 'package:ltqd/constants/colors.dart';
import 'package:ltqd/features/tracking/presentation/widgets/components/forms/sleep_form.dart';

class ChildDataEntry extends StatelessWidget {
  ChildDataEntry({Key? key}) : super();

  final List<Widget> widgets = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.black,
      body: SizedBox.expand(
        child: DraggableScrollableSheet(
          initialChildSize: 0.9,
          maxChildSize: 0.9,
          expand: true,
          builder: (BuildContext context, ScrollController scrollController) {
            return Container(
                padding: EdgeInsets.all(30),
                decoration: BoxDecoration(
                    color: AppColors.backgroundColor,
                    borderRadius: BorderRadius.all(
                      Radius.circular(30),
                    )),
                child: Column(
                  children: [
                    Divider(
                      height: 25,
                      thickness: 3,
                      color: AppColors.grey,
                      indent: 170,
                      endIndent: 170,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 280,
                          child: Text(
                            'Décrivez cette période de sommeil',
                            style: Theme.of(context).textTheme.headline1,
                          ),
                        ),
                        Expanded(
                            child:
                                Image.asset('assets/images/sleepingboy.png')),
                      ],
                    ),
                    SleepForm(),
                    TextButton(onPressed: () {}, child: Text('Enregistrer')),
                  ],
                ));
          },
        ),
      ),
    );
  }
}
