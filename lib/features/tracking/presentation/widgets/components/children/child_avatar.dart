import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ltqd/constants/colors.dart';
import 'package:ltqd/features/tracking/domain/entities/children.dart';
import 'package:ltqd/features/tracking/presentation/bloc/children/children_bloc.dart';

class ChildAvatar extends StatelessWidget {
  final Children child;
  const ChildAvatar({Key? key, required this.child}) : super(key: key);

  onTapped(context, child) {
    BlocProvider.of<ChildrenBloc>(context).add(ChildrenChildActivated(child));
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTapped(context, child),
      child: Container(
        height: 50,
        width: 50,
        margin: EdgeInsets.only(right: 7),
        decoration: BoxDecoration(
            color: child.active ? AppColors.green : AppColors.orange,
            borderRadius: BorderRadius.all(Radius.circular(30))),
        child: Center(
          child: Text(
            child.initials,
            style: TextStyle(
                fontFamily: 'Poppins',
                fontWeight: FontWeight.w900,
                fontSize: 14,
                color: AppColors.white),
          ),
        ),
      ),
    );
  }
}
