import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ltqd/core/injection_container.dart';
import 'package:ltqd/features/tracking/domain/object_values/gender.dart';
import 'package:ltqd/features/tracking/presentation/bloc/child_active/child_active_bloc.dart';
import 'package:ltqd/features/tracking/presentation/bloc/timer/timer_bloc.dart';

class ChildPicture extends StatelessWidget {
  final TimerState timerState;
  const ChildPicture({Key? key, required this.timerState}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChildActiveBloc, ChildActiveState>(
        bloc: sl<ChildActiveBloc>(),
        builder: (context, state) {
          if (state is ChildActiveActivateSuccess) {
            var isWakeup =
                timerState is TimerInitial || timerState is TimerRunStop;
            var wokeUpAssetPath = state.child.gender == Gender.boy
                ? 'assets/images/wakeupmartin.png'
                : 'assets/images/wokeupgirl.png';
            var sleepingAssetPath = state.child.gender == Gender.boy
                ? 'assets/images/sleepingmartin.png'
                : 'assets/images/sleepinggirl.png';
            var assetPath = isWakeup ? wokeUpAssetPath : sleepingAssetPath;
            return Container(
                width: 200,
                height: 200,
                decoration: BoxDecoration(
                    image: DecorationImage(
                  image: AssetImage(assetPath),
                  fit: BoxFit.contain,
                )));
          } else {
            return Text('not initialized');
          }
        });
  }
}
