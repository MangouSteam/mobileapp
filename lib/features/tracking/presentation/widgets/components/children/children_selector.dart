import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ltqd/features/tracking/presentation/bloc/children/children_bloc.dart';
import 'package:ltqd/features/tracking/presentation/widgets/components/children/child_avatar.dart';

class ChildrenSelector extends StatefulWidget {
  ChildrenSelector({Key? key}) : super(key: key);

  _ChildrenSelectorState createState() => _ChildrenSelectorState();
}

class _ChildrenSelectorState extends State<ChildrenSelector> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: BlocBuilder<ChildrenBloc, ChildrenState>(
        builder: (context, state) {
          if (state is ChildrenInitial) {
            return Text("Aucun enfant n'est enregistré");
          } else if (state is ChildrenLoadSuccess) {
            final children = state.children;
            return SizedBox(
                width: double.infinity,
                height: 50,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: children.length,
                    itemBuilder: (context, index) =>
                        ChildAvatar(child: children[index])));
          } else {
            return Text('Is loading');
          }
        },
      ),
    );
  }
}
