import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ltqd/core/injection_container.dart';
import 'package:ltqd/features/tracking/presentation/bloc/child_active/child_active_bloc.dart';

class ChildFirstName extends StatelessWidget {
  const ChildFirstName({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChildActiveBloc, ChildActiveState>(
        bloc: sl<ChildActiveBloc>(),
        builder: (context, state) {
          if (state is ChildActiveActivateSuccess) {
            return Text(
              state.child.firstName,
              style: Theme.of(context).textTheme.headline1,
            );
          } else {
            return Text('childFirstNameDefault');
          }
        });
  }
}
