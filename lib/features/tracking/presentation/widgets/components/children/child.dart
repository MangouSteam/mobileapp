import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ltqd/core/injection_container.dart';
import 'package:ltqd/features/tracking/domain/services/timer_service.dart';
import 'package:ltqd/features/tracking/presentation/bloc/child_active/child_active_bloc.dart';
import 'package:ltqd/features/tracking/presentation/bloc/timer/timer_bloc.dart';
import 'package:ltqd/features/tracking/presentation/bloc/tracking_mediator/tracking_mediator_bloc.dart';
import 'package:ltqd/features/tracking/presentation/widgets/components/children/child_firstname.dart';
import 'package:ltqd/features/tracking/presentation/widgets/components/children/child_picture.dart';
import 'package:ltqd/features/tracking/presentation/widgets/components/timer_text.dart';

class Child extends StatelessWidget {
  const Child({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _state = context.watch<ChildActiveBloc>().state;
    final _startTime = _state is ChildActiveActivateSuccess
        ? _state.child.sleepTimer!.startTime
        : DateTime.now();
    final _duration = TimerService.getDurationInSeconds(_startTime!);
    return BlocBuilder<TimerBloc, TimerState>(builder: (context, state) {
      return Column(children: [
        ChildPicture(timerState: state),
        ChildFirstName(),
        Text(
          state is TimerInitial || state is TimerRunStop
              ? 'est éveillé'
              : 'dort',
          style: Theme.of(context).textTheme.headline5,
        ),
        TimerText(),
        TextButton(
          onPressed: () {
            if (state is TimerInitial || state is TimerRunStop) {
              sl<TimerBloc>()..add(TimerStarted(duration: _duration));
              sl<TrackingMediatorBloc>()..add(MediatorTimerStarted());
            } else {
              sl<TimerBloc>()..add(TimerStopped(duration: state.duration));
              sl<TrackingMediatorBloc>()..add(MediatorTimerStopped());
            }
          },
          child: Text(state is TimerInitial || state is TimerRunStop
              ? 'Je lance le chrono'
              : 'J\'arrête le chrono'),
        ),
      ]);
    });
  }
}
