import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ltqd/constants/colors.dart';
import 'package:ltqd/core/injection_container.dart';
import 'package:ltqd/features/tracking/domain/object_values/period.dart';
import 'package:ltqd/features/tracking/presentation/bloc/period/period_bloc.dart';

class PeriodSelector extends StatelessWidget {
  PeriodSelector({Key? key}) : super(key: key);

  final List<Period> _periods = [
    Period(label: "Aujourd'hui"),
    Period(label: "Hier"),
    Period(label: "Semaine"),
    Period(label: "Mois"),
  ];

  void _onTapped(int index) {
    sl<PeriodBloc>()..add(PeriodChanged(period: _periods[index]));
  }

  bool _isSamePeriod(Period period1, Period period2) {
    return period1 == period2;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 40,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: _periods.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () => _onTapped(index),
              child: BlocBuilder<PeriodBloc, PeriodState>(
                builder: (context, state) {
                  final _period = state.period;
                  return Container(
                    decoration: BoxDecoration(
                      border: _isSamePeriod(_period, _periods[index])
                          ? Border.all(color: AppColors.yellow)
                          : Border.all(
                              color: AppColors.black,
                              width: 1,
                            ),
                      color: _isSamePeriod(_period, _periods[index])
                          ? AppColors.yellow
                          : AppColors.white,
                      borderRadius: BorderRadius.all(
                        Radius.circular(30),
                      ),
                    ),
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 15),
                    margin: EdgeInsets.only(right: 10),
                    child: Center(
                      child: Text(
                        _periods[index].label,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  );
                },
              ),
            );
          },
        ));
  }
}
