import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ltqd/constants/colors.dart';
import 'package:ltqd/features/tracking/presentation/bloc/period/period_bloc.dart';

class PeriodTitle extends StatelessWidget {
  const PeriodTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocBuilder<PeriodBloc, PeriodState>(
        builder: (context, state) {
          return Text(
            state.period.label,
            style: TextStyle(color: AppColors.purple),
          );
        },
      ),
    );
  }
}
