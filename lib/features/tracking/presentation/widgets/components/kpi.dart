import 'package:flutter/material.dart';
import 'package:ltqd/constants/colors.dart';

class Kpi extends StatelessWidget {
  final String data;
  final String label;
  const Kpi({Key? key, required this.data, required this.label})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 160,
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.fitHeight,
                image: AssetImage('assets/images/postit.png'))),
        child: Column(
          children: [
            Text(data,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Poppins',
                    color: AppColors.green,
                    fontSize: 16)),
            Text(label)
          ],
        ));
  }
}
