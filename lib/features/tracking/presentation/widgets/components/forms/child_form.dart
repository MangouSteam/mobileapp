import 'package:flutter/material.dart';
import 'package:ltqd/core/option.dart';
import 'package:ltqd/features/shared/presentation/widgets/forms/options_field.dart';
import 'package:ltqd/features/tracking/domain/object_values/gender.dart';

class ChildForm extends StatefulWidget {
  ChildForm({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ChildFormState();
  }
}

class ChildFormState extends State<ChildForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 30),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextField(
              obscureText: false,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: "Prénom de l'enfant",
              ),
            ),
            OptionsField(
              title: 'Genre :',
              collection: [
                Option(
                  label: Gender.boy,
                  asset: '',
                ),
                Option(
                  label: Gender.girl,
                  asset: '',
                )
              ],
              onSaved: () {},
            ),
            DropdownButton(items: [])
          ],
        ),
      ),
    );
  }
}
