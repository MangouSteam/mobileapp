import 'package:flutter/material.dart';
import 'package:ltqd/constants/colors.dart';
import 'package:ltqd/features/shared/presentation/widgets/forms/options_field.dart';
import 'package:ltqd/features/tracking/data/datasources/options/options_data.dart';

class SleepForm extends StatefulWidget {
  SleepForm({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SleepFormState();
  }
}

class SleepFormState extends State<SleepForm> {
  final _formKey = GlobalKey<FormState>();

  final _sleepingConditionCollection = OptionsData().all();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 30),
      child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(bottom: 20),
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: AppColors.backgroundColor,
                            border: Border.all(color: AppColors.black),
                            borderRadius:
                                BorderRadius.all(Radius.circular(15))),
                        width: 150,
                        height: 80,
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Début',
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                  )),
                              Text('Lundi 3 juin 22:03'),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: AppColors.backgroundColor,
                            border: Border.all(color: AppColors.black),
                            borderRadius:
                                BorderRadius.all(Radius.circular(15))),
                        width: 150,
                        height: 80,
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Fin',
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold)),
                              Text('Lundi 3 juin 22:03'),
                            ],
                          ),
                        ),
                      ),
                    ],
                  )),
              Container(
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Nombre de réveils nocturnes',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        )),
                    DropdownButtonFormField(
                      items: [
                        DropdownMenuItem(child: Text('0'), value: 0),
                        DropdownMenuItem(child: Text('1'), value: 1),
                        DropdownMenuItem(child: Text('2'), value: 2)
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: double.infinity,
                child: OptionsField(
                  title: "Les conditions d'endormissement",
                  collection: _sleepingConditionCollection,
                  onSaved: () {},
                ),
              ),
              SizedBox(
                width: double.infinity,
                child: OptionsField(
                  title: "Qu'a t'il fait avant de dormir ?",
                  collection: _sleepingConditionCollection,
                  onSaved: () {},
                ),
              ),
            ],
          )),
    );
  }
}
