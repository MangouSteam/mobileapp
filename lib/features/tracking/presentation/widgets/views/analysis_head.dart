import 'package:flutter/material.dart';
import 'package:ltqd/features/tracking/presentation/widgets/components/children/child_firstname.dart';
import 'package:ltqd/features/tracking/presentation/widgets/components/children/children_selector.dart';
import 'package:ltqd/features/tracking/presentation/widgets/components/period/period_selector.dart';
import 'package:ltqd/features/tracking/presentation/widgets/components/period/period_title.dart';

class AnalysisHead extends StatelessWidget {
  const AnalysisHead({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Container(
        child: Column(children: [
          ChildrenSelector(),
          ChildFirstName(),
          PeriodTitle(),
        ]),
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 0),
      ),
      Container(
        child: PeriodSelector(),
      ),
    ]);
  }
}
