import 'package:flutter/material.dart';
import 'package:ltqd/features/tracking/presentation/widgets/components/kpi.dart';

class AnalysisKpi extends StatelessWidget {
  const AnalysisKpi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 20),
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Kpi(data: "0 h 21 min", label: "d'endormissement"),
                  Kpi(data: "2", label: "réveils nocturnes"),
                ],
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Kpi(data: "13 h 21 min", label: "de sommeil nocturnes"),
                  Kpi(data: "1 h 21 min", label: "en 1 sieste"),
                ],
              ),
            )
          ],
        ));
  }
}
