import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ltqd/features/tracking/presentation/bloc/timer/timer_bloc.dart';
import 'package:ltqd/features/tracking/presentation/widgets/components/children/children_selector.dart';
import 'package:ltqd/features/tracking/presentation/widgets/components/timer_text.dart';
import 'package:ltqd/features/tracking/presentation/widgets/components/tracking_selector.dart';

class FeedTrackingView extends StatelessWidget {
  FeedTrackingView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(children: [
          ChildrenSelector(),
          Expanded(
              child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
              image: AssetImage('assets/images/background.png'),
            )),
          )),
          BlocBuilder<TimerBloc, TimerState>(builder: (context, state) {
            return Column(children: [
              if (state is TimerInitial) ...[
                Text('Gustave mange'),
                TimerText(),
                TextButton(
                  onPressed: () {
                    BlocProvider.of<TimerBloc>(context)
                        .add(TimerStarted(duration: 0));
                  },
                  child: Text('Je lance le chrono'),
                ),
              ],
              if (state is TimerRunInProgress) ...[
                Text('Gustave dort'),
                TimerText(),
                TextButton(
                  onPressed: () {
                    BlocProvider.of<TimerBloc>(context)
                        .add(TimerPaused(duration: state.duration));
                  },
                  child: Text('J\'arrête le chrono'),
                ),
              ],
              if (state is TimerRunPause) ...[
                Text('Gustave a dormi'),
                TimerText(),
                TextButton(
                  onPressed: () {
                    BlocProvider.of<TimerBloc>(context)
                        .add(TimerStarted(duration: state.duration));
                  },
                  child: Text('Je lance le chrono'),
                ),
              ],
            ]);
          }),
          TrackingSelector()
        ]),
      ),
    );
  }
}
