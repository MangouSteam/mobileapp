import 'package:equatable/equatable.dart';

class SleepSession extends Equatable {
  final String? childrenId;
  final DateTime? startTime;
  final DateTime? endTime;

  SleepSession({
    this.childrenId,
    this.startTime,
    this.endTime,
  });

  @override
  List<Object?> get props => [this.childrenId];

  @override
  String toString() {
    return "SleepSession(\n childrenId: ${this.childrenId},\n startTime: ${this.startTime},\n endTime: ${this.endTime})";
  }
}
