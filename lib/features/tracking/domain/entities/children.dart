import 'package:equatable/equatable.dart';
import 'package:ltqd/features/tracking/data/models/children.dart';
import 'package:ltqd/features/tracking/domain/object_values/sleep_timer.dart';

class Children extends Equatable {
  final String firstName;
  final String gender;
  final String initials;
  final String id;
  final bool active;
  final SleepTimer? sleepTimer;

  Children(
      {required this.firstName,
      required this.gender,
      required this.id,
      required this.active,
      this.sleepTimer,
      initials})
      : this.initials = firstName.substring(0, 2).toUpperCase();

  static Children setChildActive(Children oldState) {
    return Children(
      firstName: oldState.firstName,
      gender: oldState.gender,
      id: oldState.id,
      active: true,
      sleepTimer: oldState.sleepTimer,
    );
  }

  static List<Children> setChildrenNotActive(List<Children> listOfOldState) {
    return listOfOldState.map((oldState) {
      return Children(
        firstName: oldState.firstName,
        gender: oldState.gender,
        id: oldState.id,
        active: false,
        sleepTimer: oldState.sleepTimer,
      );
    }).toList();
  }

  toModel() {
    return ChildrenModel(
        firstName: this.firstName,
        gender: this.gender,
        id: this.id,
        initials: this.initials,
        sleepTimer: this.sleepTimer);
  }

  @override
  List<Object?> get props => [id, firstName, gender];

  @override
  String toString() {
    return "Children(\n firstName: ${this.firstName},\n gender: ${this.gender}, sleepTimer: ${this.sleepTimer})";
  }
}
