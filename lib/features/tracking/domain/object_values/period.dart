import 'package:equatable/equatable.dart';

class Period extends Equatable {
  final String label;

  Period({required this.label});

  @override
  List<Object?> get props => [label];
}
