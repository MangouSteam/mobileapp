class SleepTimer {
  final bool currentlyTicking;
  final DateTime? startTime;

  SleepTimer({required this.currentlyTicking, required this.startTime});

  @override
  String toString() {
    return "SleepTimer(\n currentlyTicking: ${this.currentlyTicking},\n startTime: ${this.startTime})";
  }
}
