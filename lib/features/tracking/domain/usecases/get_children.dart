import 'package:dartz/dartz.dart';
import 'package:ltqd/core/failure.dart';
import 'package:ltqd/core/usecases/usecase.dart';
import 'package:ltqd/features/tracking/domain/entities/children.dart';
import 'package:ltqd/features/tracking/domain/repositories/children_repository.dart';

class GetChildren implements UseCase<List<Children>, NoParams> {
  final ChildrenRepository repository;
  GetChildren(this.repository);

  @override
  Future<Either<Failure, List<Children>>> call(NoParams params) async {
    return await repository.getChildren();
  }
}
