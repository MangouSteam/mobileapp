import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:ltqd/core/failure.dart';
import 'package:ltqd/core/usecases/usecase.dart';
import 'package:ltqd/features/tracking/domain/entities/children.dart';
import 'package:ltqd/features/tracking/domain/repositories/children_repository.dart';

class GetChildById extends UseCase<Children, GetChildByIdParams> {
  final ChildrenRepository repository;
  GetChildById(this.repository);

  @override
  Future<Either<Failure, Children>> call(GetChildByIdParams params) async {
    return await repository.getChildById(id: params.id);
  }
}

class GetChildByIdParams extends Equatable {
  final String id;

  GetChildByIdParams({required this.id}) : super();

  @override
  List<Object?> get props => [id];
}
