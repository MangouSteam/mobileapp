import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:ltqd/core/failure.dart';
import 'package:ltqd/core/usecases/usecase.dart';
import 'package:ltqd/features/tracking/domain/repositories/children_repository.dart';

class EndSleepTimer extends UseCase<void, EndSleepTimerParams> {
  final ChildrenRepository repository;
  EndSleepTimer(this.repository);

  @override
  Future<Either<Failure, bool>> call(EndSleepTimerParams params) async {
    return await repository.endSleepTimer(childrenId: params.childrenId);
  }
}

class EndSleepTimerParams extends Equatable {
  final String childrenId;

  EndSleepTimerParams({required this.childrenId}) : super();

  @override
  List<Object?> get props => [childrenId];
}
