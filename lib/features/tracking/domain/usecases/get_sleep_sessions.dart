import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:ltqd/core/failure.dart';
import 'package:ltqd/core/usecases/usecase.dart';
import 'package:ltqd/features/tracking/domain/entities/sleep_session.dart';
import 'package:ltqd/features/tracking/domain/repositories/sleep_sessions_repository.dart';

class GetSleepSessions
    extends UseCase<List<SleepSession>, GetSleepSessionsParams> {
  final SleepSessionsRepository repository;
  GetSleepSessions(this.repository);

  @override
  Future<Either<Failure, List<SleepSession>>> call(
      GetSleepSessionsParams params) async {
    return await repository.getSleepSessions(
      childrenId: params.childrenId,
    );
  }
}

class SleepSessions {}

class GetSleepSessionsParams extends Equatable {
  final String childrenId;

  GetSleepSessionsParams({required this.childrenId}) : super();

  @override
  List<Object?> get props => [childrenId];
}
