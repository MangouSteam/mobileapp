import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:ltqd/core/failure.dart';
import 'package:ltqd/core/usecases/usecase.dart';
import 'package:ltqd/features/tracking/domain/entities/sleep_session.dart';
import 'package:ltqd/features/tracking/domain/repositories/sleep_sessions_repository.dart';

class GetCurrentSleepSession
    extends UseCase<List<SleepSession>, GetCurrentSleepSessionsParams> {
  final SleepSessionsRepository repository;
  GetCurrentSleepSession(this.repository);

  @override
  Future<Either<Failure, List<SleepSession>>> call(
      GetCurrentSleepSessionsParams params) async {
    return await repository.getSleepSessions(
      childrenId: params.childrenId,
    );
  }
}

class SleepSessions {}

class GetCurrentSleepSessionsParams extends Equatable {
  final String childrenId;

  GetCurrentSleepSessionsParams({required this.childrenId}) : super();

  @override
  List<Object?> get props => [childrenId];
}
