import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:ltqd/core/failure.dart';
import 'package:ltqd/core/usecases/usecase.dart';
import 'package:ltqd/features/tracking/domain/entities/children.dart';
import 'package:ltqd/features/tracking/domain/repositories/children_repository.dart';

class AddNewChild extends UseCase<void, AddNewChildParams> {
  final ChildrenRepository repository;
  AddNewChild(this.repository);

  @override
  Future<Either<Failure, bool>> call(AddNewChildParams params) async {
    return await repository.addNewChild(
      child: params.child,
    );
  }
}

class AddNewChildParams extends Equatable {
  final Children child;

  AddNewChildParams({
    required this.child,
  }) : super();

  @override
  List<Object?> get props => [];
}
