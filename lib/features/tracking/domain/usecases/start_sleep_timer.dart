import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:ltqd/core/failure.dart';
import 'package:ltqd/core/usecases/usecase.dart';
import 'package:ltqd/features/tracking/domain/repositories/children_repository.dart';

class StartSleepTimer extends UseCase<bool, StartSleepTimerParams> {
  final ChildrenRepository repository;
  StartSleepTimer(this.repository);

  @override
  Future<Either<Failure, bool>> call(StartSleepTimerParams params) async {
    return await repository.startSleepTimer(
      startTime: params.startTime,
      childrenId: params.childrenId,
    );
  }
}

class StartSleepTimerParams extends Equatable {
  final String childrenId;
  final DateTime startTime;

  StartSleepTimerParams({required this.childrenId, required this.startTime})
      : super();

  @override
  List<Object?> get props => [startTime, childrenId];
}
