import 'package:ltqd/core/usecases/usecase.dart';
import 'package:ltqd/features/tracking/domain/entities/children.dart';
import 'package:ltqd/features/tracking/domain/repositories/children_repository.dart';

class GetChildrenStream {
  final ChildrenRepository repository;
  GetChildrenStream(this.repository);

  Stream<List<Children>> call(NoParams params) {
    return repository.getChildrenStream();
  }
}
