class TimerService {
  static int getDurationInSeconds(DateTime datetime) {
    final int nowTimestamp = DateTime.now().millisecondsSinceEpoch;
    final int dateToBeSubstracted = datetime.millisecondsSinceEpoch;
    if (nowTimestamp < dateToBeSubstracted) {
      return 0;
    }
    return (nowTimestamp - dateToBeSubstracted) ~/ 1000;
  }
}
