import 'package:dartz/dartz.dart';
import 'package:ltqd/core/failure.dart';
import 'package:ltqd/features/tracking/domain/entities/sleep_session.dart';

abstract class SleepSessionsRepository {
  Future<Either<Failure, SleepSession>> getSleepSessionById({id: id});

  Future<Either<Failure, List<SleepSession>>> getSleepSessions(
      {required String childrenId});

  Future<Either<Failure, String>> startSleepSession(
      {required DateTime startTime, required String childrenId});

  Future<Either<Failure, String>> endSleepSession(
      {required DateTime endTime, required String sleepSessionId});
}
