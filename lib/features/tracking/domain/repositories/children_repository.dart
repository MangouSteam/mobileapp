import 'package:dartz/dartz.dart';
import 'package:ltqd/core/failure.dart';
import 'package:ltqd/features/tracking/domain/entities/children.dart';

abstract class ChildrenRepository {
  Future<Either<Failure, Children>> getChildById({id: id});

  Future<Either<Failure, List<Children>>> getChildren();

  Stream<List<Children>> getChildrenStream();

  Future<Either<Failure, bool>> startSleepTimer(
      {required DateTime startTime, required String childrenId});

  Future<Either<Failure, bool>> endSleepTimer({required String childrenId});

  Future<Either<Failure, bool>> addNewChild({required Children child});
}
