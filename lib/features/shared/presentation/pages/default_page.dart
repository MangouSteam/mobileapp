import 'package:flutter/material.dart';

class DefaultPage extends StatelessWidget {
  DefaultPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(children: [Text('default page')]),
      ),
    );
  }
}
