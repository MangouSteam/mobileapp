import 'package:flutter/material.dart';
import 'package:ltqd/features/shared/presentation/widgets/detail_app_bar.dart';

class DetailBasePage extends StatefulWidget {
  final Widget injected;
  DetailBasePage({Key? key, required this.injected}) : super(key: key);

  _DetailBasePageState createState() => _DetailBasePageState();
}

class _DetailBasePageState extends State<DetailBasePage> {
  var injected;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: LTQDDetailAppBar(name: ''),
      body: widget.injected,
    );
  }
}
