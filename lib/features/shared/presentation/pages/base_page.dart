import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ltqd/constants/colors.dart';
import 'package:ltqd/core/injection_container.dart';
import 'package:ltqd/custom_icons.dart';
import 'package:ltqd/features/account/presentation/pages/account_page.dart';
import 'package:ltqd/features/home/presentation/pages/home_page.dart';
import 'package:ltqd/features/shared/presentation/widgets/app_bar.dart';
import 'package:ltqd/features/tracking/presentation/bloc/child_active/child_active_bloc.dart';
import 'package:ltqd/features/tracking/presentation/bloc/children/children_bloc.dart';
import 'package:ltqd/features/tracking/presentation/bloc/period/period_bloc.dart';
import 'package:ltqd/features/tracking/presentation/bloc/timer/timer_bloc.dart';
import 'package:ltqd/features/tracking/presentation/pages/analysis_page.dart';
import 'package:ltqd/features/tracking/presentation/pages/sleep_tracking_page.dart';

class BasePage extends StatefulWidget {
  final int index;
  BasePage({Key? key, required this.index}) : super(key: key);

  _BasePageState createState() => _BasePageState();
}

class _BasePageState extends State<BasePage> {
  int _selectedIndex = 0;

  List _titles = ['Home', 'Sommeil', 'Analyse', 'Mon compte'];

  List _pages = [
    HomePage(),
    SleepTrackingPage(),
    AnalysisPage(),
    AccountPage(),
  ];

  @override
  void initState() {
    setState(() {
      _selectedIndex = widget.index;
    });
    super.initState();
  }

  void _onTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<TimerBloc>(
            create: (_) => sl<TimerBloc>(),
          ),
          BlocProvider<ChildrenBloc>(
            create: (_) => sl<ChildrenBloc>()..add(ChildrenDataLoading()),
          ),
          BlocProvider<ChildActiveBloc>(
            create: (_) => sl<ChildActiveBloc>(),
          ),
          BlocProvider<PeriodBloc>(
            create: (_) => sl<PeriodBloc>(),
          )
        ],
        child: Scaffold(
          appBar: LTQDAppBar(name: _titles[_selectedIndex]),
          bottomNavigationBar: BottomNavigationBar(
            backgroundColor: AppColors.backgroundColor,
            onTap: _onTapped,
            currentIndex: _selectedIndex,
            fixedColor: AppColors.black,
            type: BottomNavigationBarType.fixed,
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(
                  Custom.tipi,
                ),
                label: 'Home',
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Custom.trackeralt,
                ),
                label: 'Tracker',
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Custom.analysis,
                ),
                label: 'Analyse',
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Custom.account,
                ),
                label: 'Mon compte',
              )
            ],
          ),
          body: _pages[_selectedIndex],
        ));
  }
}
