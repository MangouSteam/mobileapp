import 'package:flutter/material.dart';
import 'package:ltqd/constants/colors.dart';
import 'package:ltqd/core/option.dart';

class OptionsField extends StatelessWidget {
  final String title;
  final List<Option> collection;
  final Function onSaved;
  const OptionsField(
      {Key? key,
      required this.title,
      required this.collection,
      required this.onSaved})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
        ),
        Container(
            constraints: BoxConstraints(
              maxHeight: 100,
              maxWidth: double.infinity,
            ),
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: collection.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      border: Border.all(color: AppColors.black),
                      borderRadius: BorderRadius.all(
                        Radius.circular(15),
                      ),
                    ),
                    margin: EdgeInsets.all(10),
                    width: 75,
                    child: Column(
                      children: [
                        Text(
                          collection[index].label,
                        ),
                        Image.asset(collection[index].asset),
                      ],
                    ));
              },
            )),
      ],
    );
  }
}
