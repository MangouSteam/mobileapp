import 'package:flutter/material.dart';
import 'package:ltqd/constants/colors.dart';

class LTQDDetailAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String name;

  const LTQDDetailAppBar({Key? key, required this.name}) : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(60);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      title: Text(this.name),
      leading: IconButton(
          icon: Icon(Icons.west),
          color: AppColors.black,
          onPressed: () {
            Navigator.pop(context);
          }),
    );
  }
}
