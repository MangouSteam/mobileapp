import 'package:flutter/material.dart';
import 'package:ltqd/constants/colors.dart';
import 'package:ltqd/custom_icons.dart';

class LTQDAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String name;

  const LTQDAppBar({Key? key, required this.name}) : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(60);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      title: Text(this.name),
      leading: Icon(
        Custom.bell,
        color: AppColors.black,
      ),
      actions: [
        Icon(
          Custom.message,
          color: AppColors.black,
        )
      ],
    );
  }
}
