import 'package:flutter/material.dart';

class AppColors {
  AppColors._();
  static const backgroundColor = Color(0xFFF8F6F2);
  static const purple = Color(0xFFD9DBEC);
  static const yellow = Color(0xFFEBC15E);
  static const orange = Color(0xFFF9A98B);
  static const green = Color(0xFF6C9EA5);
  static const textColor = Color(0xFF363636);
  static const black = Color(0xFF030303);
  static const grey = Color(0xFFA5A9C5);
  static const white = Color(0xFFFFFFFF);
}
