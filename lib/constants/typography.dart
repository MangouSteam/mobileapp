import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ltqd/constants/colors.dart';

class AppTypography {
  AppTypography._();

  static TextStyle h1 = GoogleFonts.abrilFatface(
      textStyle: TextStyle(
    color: AppColors.black,
    fontSize: 28,
  ));
  static TextStyle h2 = GoogleFonts.poppins(
      textStyle: TextStyle(
    color: AppColors.textColor,
    fontSize: 22,
  ));
  static TextStyle h3 = GoogleFonts.poppins(
      textStyle: TextStyle(
    color: AppColors.textColor,
    fontSize: 28,
  ));
  static TextStyle h4 = GoogleFonts.poppins(
      textStyle: TextStyle(
    color: AppColors.textColor,
    fontSize: 24,
  ));
  static TextStyle h5 = GoogleFonts.poppins(
      textStyle: TextStyle(
    color: AppColors.textColor,
    fontSize: 20,
  ));
  static TextStyle h6 = GoogleFonts.poppins(
      textStyle: TextStyle(
    color: AppColors.textColor,
    fontSize: 16,
  ));
  static TextStyle text = GoogleFonts.poppins(
      textStyle: TextStyle(
    color: AppColors.textColor,
    fontSize: 12,
  ));
}
