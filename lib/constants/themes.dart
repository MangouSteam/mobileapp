import 'package:flutter/material.dart';
import 'package:ltqd/constants/colors.dart';
import 'package:ltqd/constants/typography.dart';

class AppThemes {
  AppThemes._();

  static ThemeData lightTheme = ThemeData(
    fontFamily: 'Poppins',
    // This is the theme of your application.
    //
    // Try running your application with "flutter run". You'll see the
    // application has a blue toolbar. Then, without quitting the app, try
    // changing the primarySwatch below to Colors.green and then invoke
    // "hot reload" (press "r" in the console where you ran "flutter run",
    // or simply save your changes to "hot reload" in a Flutter IDE).
    // Notice that the counter didn't reset back to zero; the application
    // is not restarted.
    textTheme: TextTheme(
      headline1: AppTypography.h1,
      headline2: AppTypography.h2,
      headline3: AppTypography.h3,
      headline4: AppTypography.h4,
      headline5: AppTypography.h5,
      headline6: AppTypography.h6,
      button: AppTypography.text,
      bodyText1: AppTypography.text,
      bodyText2: AppTypography.text,
    ),
    bottomAppBarColor: AppColors.backgroundColor,
    bottomAppBarTheme: BottomAppBarTheme(
      color: AppColors.black,
    ),
    appBarTheme: AppBarTheme(
      titleTextStyle: TextStyle(
        color: AppColors.black,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.bold,
        fontSize: 14,
      ),
      backgroundColor: AppColors.backgroundColor,
      centerTitle: true,
      iconTheme: IconThemeData(
        color: AppColors.black,
      ),
    ),
  );
}
