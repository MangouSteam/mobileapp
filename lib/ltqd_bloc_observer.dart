import 'package:bloc/bloc.dart';

class LTQDBlocObserver extends BlocObserver {
  @override
  void onChange(BlocBase bloc, Change change) {
/*     super.onChange(bloc, change);
 */ //print('${bloc.runtimeType} $change');
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    super.onError(bloc, error, stackTrace);
    //print('/n${bloc.runtimeType} $error $stackTrace');
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    /*   super.onTransition(bloc, transition);
    print('${bloc.runtimeType} $transition'); */
  }
}
