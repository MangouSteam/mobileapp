import { firestore } from "firebase-admin";
import * as functions from "firebase-functions";

const db = new firestore.Firestore();

export const sleepSessionCompleted = functions
    .region('europe-west3')
    .firestore
    .document('sleep_sessions/{sleepSessionId}')
    .onWrite(async (change, context) => {
        const newValue = change.after.data();
        const oldValue = change.before.data();
        console.log(newValue);
        console.log(oldValue);
        const childrenId = oldValue?.childrenId;

        // Update statistic once new sleepSession is added
        const statistics = await db.collection('statistics')
            .doc(childrenId)
            .get()
    } 
    });

