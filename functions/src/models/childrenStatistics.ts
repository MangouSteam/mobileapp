class ChildrenStatistics {
    napDurationToday: number;
    napDurationYesterday: number;
    napDurationWeek: number;
    napDurationMonth: number;
    numberOfNapToday: number;
    numberOfNapYesterday: number;
    numberOfNapWeek: number;
    numberOfNapMonth: number;
    fallAsleepDurationToday: number;
    fallAsleepDurationYesterday: number;
    fallAsleepDurationMonth: number;
    fallAsleepDurationWeek: number;
    numberOfAwakeningToday: number;
    numberOfAwakeningYesterday: number;
    numberOfAwakeningWeek: number;
    numberOfAwakeningMonth: number;
    numberOfData: number;

    constructor();
}